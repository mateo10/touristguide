package pl.touristguide.springapp.web.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.touristguide.springapp.user.UserFacade;
import pl.touristguide.springapp.user.dto.AccountDTO;
import pl.touristguide.springapp.user.dto.UserDetailDTO;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@Scope("request")
@CrossOrigin(origins = "*")
@RequestMapping(value = "/user")
public class UserRestController {
    private final UserFacade userFacade;

    @Autowired
    public UserRestController(UserFacade userFacade) {
        this.userFacade = userFacade;
    }

    @PostMapping(value = "/signup")
    @ResponseBody
    private ResponseEntity signup(@RequestBody UserDetailDTO userDetailDTO){
        try {
            UserDetailDTO userDetailResult = userFacade.registration(userDetailDTO);
            return new ResponseEntity(userDetailResult, HttpStatus.OK);
        }
        catch(Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PatchMapping()
    private ResponseEntity updateUserAccount(@RequestBody UserDetailDTO userDetailDTO){
        try {
            UserDetailDTO currentUser = this.userFacade.updateUserDetail(userDetailDTO);
            return new ResponseEntity(currentUser, HttpStatus.OK);
        }
        catch(Exception e){
            e.printStackTrace();
            return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @PatchMapping(value = "/password")
    private ResponseEntity updateUserAccountPassword(@RequestBody AccountDTO accountDTO) {
        try {
            this.userFacade.updateUserAccountPassword(accountDTO);
            return new ResponseEntity(HttpStatus.OK);
        }
        catch(Exception e){
            e.printStackTrace();
            return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping()
    private ResponseEntity deactivateAccount(HttpServletRequest request, HttpServletResponse response) {
        try {
            this.userFacade.deactivateAccount(request, response);
            return new ResponseEntity(HttpStatus.OK);
        }
        catch(Exception e){
            e.printStackTrace();
            return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value = "/detail")
    public ResponseEntity getAccountUserDTO() {
        try {
            UserDetailDTO userDetail = userFacade.findAccountDetails();
            return new ResponseEntity(userDetail, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "/login")
    @ResponseBody
    private ResponseEntity login(@RequestBody AccountDTO accountDTO, HttpServletRequest request, HttpServletResponse response) {
        try {
            UserDetailDTO userDetailDTO = userFacade.login(accountDTO, request);
            return new ResponseEntity(userDetailDTO, HttpStatus.OK);
        }
        catch(Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "/logout")
    public ResponseEntity logoutUser(HttpServletRequest request, HttpServletResponse response) {
        try {
            userFacade.logoutUser(request, response);
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
