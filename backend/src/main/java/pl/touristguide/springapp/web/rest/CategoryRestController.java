package pl.touristguide.springapp.web.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.touristguide.springapp.category.CategoryFacade;
import pl.touristguide.springapp.category.dto.CategoryDTO;

import java.util.List;

@RestController
@Scope("request")
@CrossOrigin(origins = "*")
@RequestMapping(value = "/categories")
public class CategoryRestController {
    private final CategoryFacade categoryFacade;

    @Autowired
    public CategoryRestController(CategoryFacade categoryFacade) {
        this.categoryFacade = categoryFacade;
    }

    @GetMapping()
    private ResponseEntity getCategories(){
        try {
            List<CategoryDTO> categoryDTOList = this.categoryFacade.getCategories();
            return new ResponseEntity(categoryDTOList, HttpStatus.OK);
        }
        catch(Exception e){
            e.printStackTrace();
            return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping()
    private ResponseEntity insertCategory(@RequestBody CategoryDTO categoryDTO) {
        try {
            CategoryDTO addedCategory = this.categoryFacade.insertCategory(categoryDTO);
            return new ResponseEntity(addedCategory, HttpStatus.OK);
        }
        catch(Exception e){
            e.printStackTrace();
            return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PatchMapping(value="/{categoryId}")
    private ResponseEntity updateCategory(@PathVariable Long categoryId, @RequestBody CategoryDTO categoryDTO) {
        try {
            CategoryDTO updatedCategory = this.categoryFacade.updateCategory(categoryId, categoryDTO);
            return new ResponseEntity(updatedCategory, HttpStatus.OK);
        }
        catch(Exception e){
            e.printStackTrace();
            return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value="/active")
    private ResponseEntity findActiveCategories() {
        try {
            List<CategoryDTO> categories = this.categoryFacade.findActiveCategories();
            return new ResponseEntity(categories, HttpStatus.OK);
        }
        catch(Exception e){
            e.printStackTrace();
            return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
