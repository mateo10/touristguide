package pl.touristguide.springapp.web.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.touristguide.springapp.occurrence.OccurrenceFacade;
import pl.touristguide.springapp.occurrence.dto.OccurrenceDTO;

import java.time.LocalDate;
import java.util.List;

@RestController
@Scope("request")
@CrossOrigin(origins = "*")
@RequestMapping(value = "/occurrences")
public class OccurrenceRestController {
    private final OccurrenceFacade occurrenceFacade;

    @Autowired
    public OccurrenceRestController(OccurrenceFacade occurrenceFacade) {
        this.occurrenceFacade = occurrenceFacade;
    }

    @PostMapping(value = "/user/{accountId}")
    private ResponseEntity findUserUccurrences(@PathVariable("accountId") Long accountId, @RequestBody LocalDate date) {
        try {
            List<OccurrenceDTO> foundOccurrences = this.occurrenceFacade.findUserOccurrences(accountId, date);
            return new ResponseEntity(foundOccurrences, HttpStatus.OK);
        }
        catch(Exception e){
            e.printStackTrace();
            return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PatchMapping()
    private ResponseEntity insertOrUpdateOccurrence(@RequestBody OccurrenceDTO occurrenceDTO) {
        try {
            OccurrenceDTO modifiedOccurrence = occurrenceFacade.insertOrUpdateOccurrence(occurrenceDTO);
            return new ResponseEntity(modifiedOccurrence, HttpStatus.OK);
        }
        catch(Exception e){
            e.printStackTrace();
            return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping(value = "/{occurrenceId}")
    private ResponseEntity deleteOccurrence(@PathVariable("occurrenceId") Long occurrenceId) {
        try {
            occurrenceFacade.deleteOccurrence(occurrenceId);
            return new ResponseEntity(HttpStatus.OK);
        }
        catch(Exception e){
            e.printStackTrace();
            return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
