package pl.touristguide.springapp.web.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.touristguide.springapp.evaluation.dto.EvaluationDTO;
import pl.touristguide.springapp.route.RouteFacade;
import pl.touristguide.springapp.route.dto.RouteDTO;
import pl.touristguide.springapp.route.dto.RouteEvaluationDTO;
import pl.touristguide.springapp.route.dto.search.RouteSearchParamsDTO;

import java.util.List;

@RestController
@Scope("request")
@CrossOrigin(origins = "*")
@RequestMapping(value = "/routes")
public class RouteRestController {
    private final RouteFacade routeFacade;

    @Autowired
    public RouteRestController(RouteFacade routeFacade) {
        this.routeFacade = routeFacade;
    }

    @GetMapping(value = "/user/{accountId}")
    private ResponseEntity getUserRoutes(@PathVariable("accountId") Long accountId) {
        try {
            List<RouteDTO> routeDTOList = this.routeFacade.getUserRoutes(accountId);
            return new ResponseEntity(routeDTOList, HttpStatus.OK);
        }
        catch(Exception e) {
            e.printStackTrace();
            return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/active")
    private ResponseEntity getRoutes() {
        try {
            List<RouteDTO> routeDTOList = this.routeFacade.getActiveRoutes();
            return new ResponseEntity(routeDTOList, HttpStatus.OK);
        }
        catch(Exception e) {
            e.printStackTrace();
            return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value = "/{routeId}")
    private ResponseEntity getRoute(@PathVariable("routeId") Long routeId) {
        try {
            RouteDTO routeDTO = this.routeFacade.getRoute(routeId);
            return new ResponseEntity(routeDTO, HttpStatus.OK);
        }
        catch(Exception e) {
            e.printStackTrace();
            return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping()
    private ResponseEntity insertRoute(@RequestBody RouteDTO routeDTO) {
        try {
            RouteDTO addedRoute = this.routeFacade.insertRoute(routeDTO);
            return new ResponseEntity(addedRoute, HttpStatus.OK);
        }
        catch(Exception e) {
            e.printStackTrace();
            return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PatchMapping(value = "/{routeId}")
    private ResponseEntity updatePlace(@PathVariable("routeId") Long routeId, @RequestBody RouteDTO routeDTO) {
        try {
            RouteDTO updatedRoute = this.routeFacade.updateRoute(routeId, routeDTO);
            return new ResponseEntity(updatedRoute, HttpStatus.OK);
        }
        catch(Exception e) {
            e.printStackTrace();
            return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping(value = "/{routeId}")
    private ResponseEntity deleteRoute(@PathVariable("routeId") Long routeId) {
        try {
            this.routeFacade.deactivateRoute(routeId);
            return new ResponseEntity(HttpStatus.OK);
        }
        catch(Exception e) {
            e.printStackTrace();
            return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @PostMapping(value = "/evaluations")
    private ResponseEntity insertEvaluation(@RequestBody RouteEvaluationDTO routeEvaluationDTO) {
        try {
            EvaluationDTO addedEvaluation = this.routeFacade.insertRouteEvaluation(routeEvaluationDTO);
            return new ResponseEntity(addedEvaluation, HttpStatus.OK);
        }
        catch(Exception e) {
            e.printStackTrace();
            return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/search")
    private ResponseEntity searchRoutes(@RequestBody RouteSearchParamsDTO searchParams) {
        try {
            List<RouteDTO> routeDTOList = this.routeFacade.searchRoutes(searchParams);
            return new ResponseEntity(routeDTOList, HttpStatus.OK);
        }
        catch(Exception e) {
            e.printStackTrace();
            return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
