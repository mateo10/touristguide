package pl.touristguide.springapp.web.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.touristguide.springapp.evaluation.dto.EvaluationDTO;
import pl.touristguide.springapp.place.PlaceFacade;
import pl.touristguide.springapp.place.dto.PlaceDTO;
import pl.touristguide.springapp.place.dto.PlaceEvaluationDTO;
import pl.touristguide.springapp.place.dto.search.PlaceSearchParamsDTO;

import java.util.List;

@RestController
@Scope("request")
@CrossOrigin(origins = "*")
@RequestMapping(value = "/places")
public class PlaceRestController {
    private final PlaceFacade placeFacade;

    @Autowired
    public PlaceRestController(PlaceFacade placeFacade) {
        this.placeFacade = placeFacade;
    }

    @GetMapping()
    private ResponseEntity getPlaces(){
        try {
            List<PlaceDTO> placeDTOList = this.placeFacade.getPlaces();
            return new ResponseEntity(placeDTOList, HttpStatus.OK);
        }
        catch(Exception e) {
            e.printStackTrace();
            return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value = "/{placeId}")
    private ResponseEntity getPlace(@PathVariable("placeId") Long placeId){
        try {
            PlaceDTO placeDTO = this.placeFacade.getPlace(placeId);
            return new ResponseEntity(placeDTO, HttpStatus.OK);
        }
        catch(Exception e) {
            e.printStackTrace();
            return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping()
    private ResponseEntity insertPlace(@RequestBody PlaceDTO placeDTO){
        try {
            PlaceDTO addedPlace = this.placeFacade.insertPlace(placeDTO);
            return new ResponseEntity(addedPlace, HttpStatus.OK);
        }
        catch(Exception e) {
            e.printStackTrace();
            return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PatchMapping(value = "/{placeId}")
    private ResponseEntity updatePlace(@PathVariable("placeId") Long placeId, @RequestBody PlaceDTO placeDTO){
        try {
            PlaceDTO updatedPlace = this.placeFacade.updatePlace(placeId, placeDTO);
            return new ResponseEntity(updatedPlace, HttpStatus.OK);
        }
        catch(Exception e) {
            e.printStackTrace();
            return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping(value = "/{placeId}")
    private ResponseEntity deletePlace(@PathVariable("placeId") Long placeId){
        try {
            this.placeFacade.deactivatePlace(placeId);
            return new ResponseEntity(HttpStatus.OK);
        }
        catch(Exception e) {
            e.printStackTrace();
            return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "/search")
    private ResponseEntity searchPlaces(@RequestBody PlaceSearchParamsDTO searchParams) {
        try {
            List<PlaceDTO> placeDTOList = this.placeFacade.searchPlaces(searchParams);
            return new ResponseEntity(placeDTOList, HttpStatus.OK);
        }
        catch(Exception e) {
            e.printStackTrace();
            return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value = "/search/category/{categoryId}")
    private ResponseEntity searchPlacesByCategoryId(@PathVariable("categoryId") Long categoryId){
        try {
            List<PlaceDTO> placeDTOList = this.placeFacade.searchPlacesByCategoryId(categoryId);
            return new ResponseEntity(placeDTOList, HttpStatus.OK);
        }
        catch(Exception e) {
            e.printStackTrace();
            return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value = "/search/user/{accountId}")
    private ResponseEntity searchPlacesByAccountId(@PathVariable("accountId") Long accountId){
        try {
            List<PlaceDTO> placeDTOList = this.placeFacade.searchPlacesByAccountId(accountId);
            return new ResponseEntity(placeDTOList, HttpStatus.OK);
        }
        catch(Exception e) {
            e.printStackTrace();
            return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PostMapping(value = "/evaluations")
    private ResponseEntity insertPlaceEvaluation(@RequestBody PlaceEvaluationDTO placeEvaluationDTO){
        try {
            EvaluationDTO evaluationDTO = this.placeFacade.insertPlaceEvaluation(placeEvaluationDTO);
            return new ResponseEntity(evaluationDTO, HttpStatus.OK);
        }
        catch(Exception e) {
            e.printStackTrace();
            return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
