package pl.touristguide.springapp.category;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.touristguide.model.Category;
import pl.touristguide.springapp.category.dto.CategoryDTO;

import java.util.List;

@Service
public class CategoryFacade {
    private final CategoryService categoryService;

    @Autowired
    public CategoryFacade(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    public List<CategoryDTO> getCategories() throws Exception {
        return categoryService.getCategories();
    }

    public Category getCategory(Long categoryId) throws Exception {
        return categoryService.getCategory(categoryId);
    }

    public CategoryDTO insertCategory(CategoryDTO categoryDTO) throws Exception {
        return categoryService.insertCategory(categoryDTO);
    }

    public CategoryDTO updateCategory(Long categoryId, CategoryDTO categoryDTO) {
        return categoryService.updateCategory(categoryId, categoryDTO);
    }

    public List<CategoryDTO> findActiveCategories() {
        return categoryService.findActiveCategories();
    }

    public List<Category> insertCategories(List<Category> categories) {
        return categoryService.insertCategories(categories);
    }
}
