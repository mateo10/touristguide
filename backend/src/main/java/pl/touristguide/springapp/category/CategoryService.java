package pl.touristguide.springapp.category;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.touristguide.model.Category;
import pl.touristguide.springapp.category.dto.CategoryDTO;

import java.util.ArrayList;
import java.util.List;

@Service
class CategoryService {
    private final CategoryDao categoryDao;

    @Autowired
    CategoryService(CategoryDao categoryDao) {
        this.categoryDao = categoryDao;
    }

    List<CategoryDTO> getCategories() throws Exception {
        Iterable<Category> categoryIterable = this.categoryDao.findAll();
        List<CategoryDTO> categoryDTOList = new ArrayList<>();

        for(Category tmpCategory : categoryIterable){
            categoryDTOList.add(CategoryMapper.toCategoryDTO(tmpCategory));
        }

        return categoryDTOList;
    }

    Category getCategory(Long categoryId) throws Exception {
        return categoryDao.findById(categoryId).get();
    }

    CategoryDTO insertCategory(CategoryDTO categoryDTO) throws Exception {
        Category category = this.categoryDao.save(CategoryMapper.toCategory(categoryDTO));
        return CategoryMapper.toCategoryDTO(category);
    }

    CategoryDTO updateCategory(Long categoryId, CategoryDTO categoryDTO) {
        categoryDTO.setCategoryId(categoryId);
        Category category = this.categoryDao.save(CategoryMapper.toCategory(categoryDTO));
        return CategoryMapper.toCategoryDTO(category);
    }

    void deleteCategory(Long categoryId) {
        this.categoryDao.deleteById(categoryId);
    }

    List<CategoryDTO> findActiveCategories() {
        Iterable<Category> categoryIterable = this.categoryDao.findCategoriesByActiveTrue();
        List<CategoryDTO> categoryDTOList = new ArrayList<>();

        for(Category tmpCategory : categoryIterable){
            categoryDTOList.add(CategoryMapper.toCategoryDTO(tmpCategory));
        }

        return categoryDTOList;
    }

    List<Category> insertCategories(List<Category> categories) {
        Iterable<Category> insertedCategories = categoryDao.saveAll(categories);
        List<Category> categoryList = new ArrayList<>();

        for(Category category : insertedCategories) {
            categoryList.add(category);
        }

        return categoryList;
    }
}
