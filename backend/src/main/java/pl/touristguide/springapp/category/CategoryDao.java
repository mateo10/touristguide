package pl.touristguide.springapp.category;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.touristguide.model.Category;

@Repository
interface CategoryDao extends CrudRepository<Category, Long> {
    Iterable<Category> findCategoriesByActiveTrue();
}
