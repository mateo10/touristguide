package pl.touristguide.springapp.category;

import pl.touristguide.model.Category;
import pl.touristguide.springapp.category.dto.CategoryDTO;

public class CategoryMapper {
    public static Category toCategory(CategoryDTO categoryDTO) {
        Category category = new Category();
        category.setCategoryId(categoryDTO.getCategoryId());
        category.setName(categoryDTO.getName());
        category.setActive(categoryDTO.isActive());
        return category;
    }

    public static CategoryDTO toCategoryDTO(Category category) {
        CategoryDTO categoryDTO = new CategoryDTO();
        categoryDTO.setCategoryId(category.getCategoryId());
        categoryDTO.setName(category.getName());
        categoryDTO.setActive(category.isActive());
        return categoryDTO;
    }
}
