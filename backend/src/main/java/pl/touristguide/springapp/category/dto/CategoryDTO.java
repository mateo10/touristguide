package pl.touristguide.springapp.category.dto;

import java.io.Serializable;

public class CategoryDTO implements Serializable {
    private Long categoryId;
    private String name;
    private boolean active;

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}

