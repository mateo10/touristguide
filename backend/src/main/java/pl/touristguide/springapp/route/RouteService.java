package pl.touristguide.springapp.route;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.touristguide.model.Account;
import pl.touristguide.model.Route;
import pl.touristguide.springapp.evaluation.EvaluationSummaryService;
import pl.touristguide.springapp.evaluation.dto.EvaluationsSummaryDTO;
import pl.touristguide.springapp.route.dto.RouteDTO;
import pl.touristguide.springapp.route.dto.search.OrderField;
import pl.touristguide.springapp.route.dto.search.Ownership;
import pl.touristguide.springapp.route.dto.search.RouteSearchParamsDTO;
import pl.touristguide.springapp.user.UserFacade;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
class RouteService {
    private final RouteDao routeDao;
    private final RouteRepository routeRepository;
    private final UserFacade userFacade;
    private final EvaluationSummaryService evaluationSummaryService;

    @Autowired
    RouteService(RouteDao routeDao, RouteRepository routeRepository, UserFacade userFacade,
                 EvaluationSummaryService evaluationSummaryService) {
        this.routeDao = routeDao;
        this.routeRepository = routeRepository;
        this.userFacade = userFacade;
        this.evaluationSummaryService = evaluationSummaryService;
    }

    List<RouteDTO> getUserRoutes(Long accountId) throws Exception {
        Account account = this.userFacade.findAccountById(accountId);
        return account.getRoutes()
                .stream()
                .map(this::buildRouteDTO)
                .collect(Collectors.toList());
    }

    List<RouteDTO> getActiveRoutes() {
        Iterable<Route> foundRoutes = routeDao.findRoutesByActiveTrue();
        return StreamSupport.stream(foundRoutes.spliterator(), false)
                .map(this::buildRouteDTO)
                .collect(Collectors.toList());
    }

    RouteDTO getRoute(Long routeId) {
        return buildRouteDTO(getRouteModel(routeId));
    }

    Route getRouteModel(Long routeId) {
        return routeDao.findById(routeId).orElse(null);
    }

    RouteDTO insertRoute(RouteDTO routeDTO) throws Exception {
        Account account = userFacade.findAccountById(routeDTO.getAccountId());
        return buildRouteDTO(routeDao.save(RouteMapper.toRoute(routeDTO, account)));
    }

    RouteDTO updateRoute(Long routeId, RouteDTO routeDTO) throws Exception {
        Account account = userFacade.findAccountById(routeDTO.getAccountId());
        return RouteMapper.toRouteDTO(this.routeDao.save(RouteMapper.toRoute(routeDTO, account)));
    }

    void deleteRoute(Long routeId) throws Exception {
        this.routeDao.deleteById(routeId);
    }

    void deactivateRoute(Long routeId) {
//        this.routeDao.deactivateRoute(routeId);
    }

    List<RouteDTO> searchRoutes(RouteSearchParamsDTO searchParams) {
        Long currentAccountId = null;
        if(searchParams.getOwnership() == Ownership.MY) {
            currentAccountId = userFacade.findAccountDetails().getAccount().getAccountId();
        }
        List<Route> routes = routeRepository.searchRoutes(searchParams, currentAccountId);

        return routes.stream()
                .map(this::buildRouteDTO)
                .sorted(getRoutesRatingComparator(searchParams))
                .collect(Collectors.toList());
    }

    private Comparator<RouteDTO> getRoutesRatingComparator(RouteSearchParamsDTO searchParams) {
        if(searchParams.getOrderField() == OrderField.RATE_ASC) {
            return Comparator.comparing(RouteDTO::getEvaluationsSummary, getAverageRatingComparator());
        }
        else if(searchParams.getOrderField() == OrderField.RATE_DESC) {
            return Comparator.comparing(RouteDTO::getEvaluationsSummary, getAverageRatingComparator()).reversed();
        }
        else {
            return (a1, a2) -> 0;
        }
    }

    private Comparator<EvaluationsSummaryDTO> getAverageRatingComparator() {
        return Comparator.comparingDouble(EvaluationsSummaryDTO::getAverageRating);
    }

    private RouteDTO buildRouteDTO(Route route) {
        RouteDTO routeDTO = RouteMapper.toRouteDTO(route);
        routeDTO.setEvaluationsSummary(evaluationSummaryService.summarizeEvaluations(routeDTO.getEvaluations()));
        routeDTO.getPlaces().forEach(place ->
                place.setEvaluationsSummary(evaluationSummaryService.summarizeEvaluations(place.getEvaluations())));
        return routeDTO;
    }
}
