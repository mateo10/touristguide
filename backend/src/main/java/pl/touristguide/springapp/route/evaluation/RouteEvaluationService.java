package pl.touristguide.springapp.route.evaluation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.touristguide.model.Account;
import pl.touristguide.model.Route;
import pl.touristguide.model.RouteEvaluation;
import pl.touristguide.springapp.evaluation.EvaluationMapper;
import pl.touristguide.springapp.evaluation.dto.EvaluationDTO;
import pl.touristguide.springapp.route.dto.RouteEvaluationDTO;
import pl.touristguide.springapp.user.UserFacade;

@Service
public class RouteEvaluationService {
    private final RouteEvaluationDao routeEvaluationDao;
    private final UserFacade userFacade;

    @Autowired
    public RouteEvaluationService(RouteEvaluationDao routeEvaluationDao, UserFacade userFacade) {
        this.routeEvaluationDao = routeEvaluationDao;
        this.userFacade = userFacade;
    }

    public EvaluationDTO insertRouteEvaluation(RouteEvaluationDTO routeEvaluationDTO, Route route) throws Exception {
        Account account = userFacade.findAccountById(routeEvaluationDTO.getAccountId());
        RouteEvaluation routeEvaluation = RouteEvaluationMapper.toRouteEvaluation(routeEvaluationDTO, route, account);
        return EvaluationMapper.routeEvaluationToEvaluationDTO(routeEvaluationDao.save(routeEvaluation));
    }
}
