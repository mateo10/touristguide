package pl.touristguide.springapp.route.dto.search;

public class RouteSearchParamsDTO {
    private String name;
    private Ownership ownership;
    private OrderField orderField;

    public String getName() {
        return name;
    }

    public Ownership getOwnership() {
        return ownership;
    }

    public OrderField getOrderField() {
        return orderField;
    }
}
