package pl.touristguide.springapp.route;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import pl.touristguide.model.Route;
import pl.touristguide.springapp.route.dto.search.OrderField;
import pl.touristguide.springapp.route.dto.search.Ownership;
import pl.touristguide.springapp.route.dto.search.RouteSearchParamsDTO;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Repository
public class RouteRepositoryImpl implements RouteRepository {
    private final EntityManager entityManager;

    @Autowired
    public RouteRepositoryImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public List<Route> searchRoutes(RouteSearchParamsDTO searchParams, Long accountId) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Route> criteriaQuery = criteriaBuilder.createQuery(Route.class);

        Root<Route> route = criteriaQuery.from(Route.class);
        List<Predicate> predicates = new ArrayList<>();

        if(searchParams.getName() != null && !searchParams.getName().isEmpty()) {
            predicates.add(criteriaBuilder.like(route.get("name"), "%" + searchParams.getName() + "%"));
        }
        if(searchParams.getOwnership() == Ownership.MY) {
            predicates.add(criteriaBuilder.equal(route.get("account").get("accountId"), accountId));
        }
        
        criteriaQuery.where(predicates.toArray(new Predicate[0]));

        if(searchParams.getOrderField() == OrderField.NAME_ASC) {
            criteriaQuery.orderBy(criteriaBuilder.asc(route.get("name")));
        }
        else if(searchParams.getOrderField() == OrderField.NAME_DESC) {
            criteriaQuery.orderBy(criteriaBuilder.desc(route.get("name")));
        }

        return entityManager.createQuery(criteriaQuery).getResultList();
    }
}
