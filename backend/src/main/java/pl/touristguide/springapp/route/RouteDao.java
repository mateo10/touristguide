package pl.touristguide.springapp.route;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.touristguide.model.Route;

@Repository
interface RouteDao extends CrudRepository<Route, Long> {
    Iterable<Route> findRoutesByActiveTrue();

    /*@Modifying
    @Query("UPDATE Route r SET r.active = false WHERE r.routeId = :routeId")
    void deactivateRoute(@Param("routeId") Long routeId);*/
}
