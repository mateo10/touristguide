package pl.touristguide.springapp.route.dto.search;

public enum OrderField {
    NAME_ASC, NAME_DESC, RATE_ASC, RATE_DESC
}
