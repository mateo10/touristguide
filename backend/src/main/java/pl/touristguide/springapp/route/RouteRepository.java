package pl.touristguide.springapp.route;

import pl.touristguide.model.Route;
import pl.touristguide.springapp.route.dto.search.RouteSearchParamsDTO;

import java.util.List;

public interface RouteRepository {
    List<Route> searchRoutes(RouteSearchParamsDTO searchParams, Long accountId);
}
