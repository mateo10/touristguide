package pl.touristguide.springapp.route.dto;

import pl.touristguide.springapp.evaluation.dto.EvaluationDTO;
import pl.touristguide.springapp.evaluation.dto.EvaluationsSummaryDTO;
import pl.touristguide.springapp.place.dto.PlaceDTO;

import java.io.Serializable;
import java.util.List;
import java.util.Queue;

public class RouteDTO implements Serializable {
    private Long routeId;
    private String name;
    private Long accountId;
    private Long distance;
    private Long duration;
    private boolean active;
    private boolean isPrivate;
    private Queue<PlaceDTO> places;
    private List<EvaluationDTO> evaluations;
    private EvaluationsSummaryDTO evaluationsSummary;

    public Long getRouteId() {
        return routeId;
    }

    public void setRouteId(Long routeId) {
        this.routeId = routeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public Long getDistance() {
        return distance;
    }

    public void setDistance(Long distance) {
        this.distance = distance;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isPrivate() {
        return isPrivate;
    }

    public void setPrivate(boolean aPrivate) {
        isPrivate = aPrivate;
    }

    public Queue<PlaceDTO> getPlaces() {
        return places;
    }

    public void setPlaces(Queue<PlaceDTO> places) {
        this.places = places;
    }

    public List<EvaluationDTO> getEvaluations() {
        return evaluations;
    }

    public void setEvaluations(List<EvaluationDTO> evaluations) {
        this.evaluations = evaluations;
    }

    public EvaluationsSummaryDTO getEvaluationsSummary() {
        return evaluationsSummary;
    }

    public void setEvaluationsSummary(EvaluationsSummaryDTO evaluationsSummary) {
        this.evaluationsSummary = evaluationsSummary;
    }
}
