package pl.touristguide.springapp.route.dto.search;

public enum Ownership {
    MY, ALL
}
