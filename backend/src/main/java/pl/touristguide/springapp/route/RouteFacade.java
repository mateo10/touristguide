package pl.touristguide.springapp.route;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.touristguide.model.Route;
import pl.touristguide.springapp.evaluation.dto.EvaluationDTO;
import pl.touristguide.springapp.route.dto.RouteDTO;
import pl.touristguide.springapp.route.dto.RouteEvaluationDTO;
import pl.touristguide.springapp.route.dto.search.RouteSearchParamsDTO;
import pl.touristguide.springapp.route.evaluation.RouteEvaluationService;

import java.util.List;

@Service
public class RouteFacade {
    private final RouteService routeService;
    private final RouteEvaluationService routeEvaluationService;

    @Autowired
    public RouteFacade(RouteService routeService, RouteEvaluationService routeEvaluationService) {
        this.routeService = routeService;
        this.routeEvaluationService = routeEvaluationService;
    }

    public List<RouteDTO> getUserRoutes(Long accountId) throws Exception {
        return routeService.getUserRoutes(accountId);
    }

    public List<RouteDTO> getActiveRoutes() throws Exception {
        return routeService.getActiveRoutes();
    }

    public RouteDTO getRoute(Long routeId) throws Exception {
        return routeService.getRoute(routeId);
    }

    public Route getRouteModel(Long routeId) throws Exception {
        return routeService.getRouteModel(routeId);
    }

    public RouteDTO insertRoute(RouteDTO routeDTO) throws Exception {
        return routeService.insertRoute(routeDTO);
    }

    public RouteDTO updateRoute(Long routeId, RouteDTO routeDTO) throws Exception {
        return routeService.updateRoute(routeId, routeDTO);
    }

    public void deactivateRoute(Long routeId) throws Exception {
        routeService.deactivateRoute(routeId);
    }

    public EvaluationDTO insertRouteEvaluation(RouteEvaluationDTO routeEvaluationDTO) throws Exception {
        Route route = getRouteModel(routeEvaluationDTO.getRouteId());
        return routeEvaluationService.insertRouteEvaluation(routeEvaluationDTO, route);
    }

    public List<RouteDTO> searchRoutes(RouteSearchParamsDTO searchParams) {
        return routeService.searchRoutes(searchParams);
    }
}
