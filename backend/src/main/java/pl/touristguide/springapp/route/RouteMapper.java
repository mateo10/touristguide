package pl.touristguide.springapp.route;

import pl.touristguide.model.Account;
import pl.touristguide.model.Route;
import pl.touristguide.springapp.route.dto.RouteDTO;
import pl.touristguide.springapp.place.PlaceMapper;
import pl.touristguide.springapp.evaluation.EvaluationMapper;

import java.math.BigDecimal;
import java.util.stream.Collectors;

public class RouteMapper {
    public static Route toRoute(RouteDTO routeDTO, Account account) {
        Route route = new Route();
        route.setRouteId(routeDTO.getRouteId());
        route.setName(routeDTO.getName());
        route.setAccount(account);
        route.setDistance(routeDTO.getDistance());
        route.setDuration(routeDTO.getDuration());
        route.setActive(routeDTO.isActive());
        route.setPrivate(routeDTO.isPrivate());
        route.setPlaces(PlaceMapper.toPlaceList(routeDTO.getPlaces()));
        return route;
    }

    public static RouteDTO toRouteDTO(Route route) {
        RouteDTO routeDTO = new RouteDTO();
        routeDTO.setRouteId(route.getRouteId());
        routeDTO.setName(route.getName());
        routeDTO.setDistance(route.getDistance());
        routeDTO.setDuration(route.getDuration());
        routeDTO.setActive(route.isActive());
        routeDTO.setPrivate(route.isPrivate());
        routeDTO.setPlaces(PlaceMapper.toPlaceDTOQueue(route.getPlaces()));
        if(route.getEvaluations() != null) {
            routeDTO.setEvaluations(route.getEvaluations().stream()
                    .map(EvaluationMapper::routeEvaluationToEvaluationDTO)
                    .collect(Collectors.toList()));
        }
        return routeDTO;
    }
}
