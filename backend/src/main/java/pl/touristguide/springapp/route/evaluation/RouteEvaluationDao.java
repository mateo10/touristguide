package pl.touristguide.springapp.route.evaluation;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.touristguide.model.RouteEvaluation;

@Repository
public interface RouteEvaluationDao extends CrudRepository<RouteEvaluation, Long> {
}
