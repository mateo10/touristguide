package pl.touristguide.springapp.route.evaluation;

import pl.touristguide.model.Account;
import pl.touristguide.model.Route;
import pl.touristguide.model.RouteEvaluation;
import pl.touristguide.springapp.route.dto.RouteEvaluationDTO;

import java.time.LocalDateTime;

public class RouteEvaluationMapper {

    public static RouteEvaluation toRouteEvaluation(RouteEvaluationDTO routeEvaluationDTO,
                                                    Route route, Account account) {
        RouteEvaluation routeEvaluation = new RouteEvaluation();
        routeEvaluation.setCommentary(routeEvaluationDTO.getComment());
        routeEvaluation.setRate(routeEvaluationDTO.getRate());
        routeEvaluation.setInsertionDate(LocalDateTime.now());
        routeEvaluation.setRoute(route);
        routeEvaluation.setAccount(account);
        return routeEvaluation;
    }
}
