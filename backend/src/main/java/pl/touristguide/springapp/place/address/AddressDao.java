package pl.touristguide.springapp.place.address;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.touristguide.model.Address;

@Repository
interface AddressDao extends CrudRepository<Address, Long> {
}
