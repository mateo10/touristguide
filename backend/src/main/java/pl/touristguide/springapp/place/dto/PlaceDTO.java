package pl.touristguide.springapp.place.dto;

import pl.touristguide.springapp.category.dto.CategoryDTO;
import pl.touristguide.springapp.evaluation.dto.EvaluationDTO;
import pl.touristguide.springapp.evaluation.dto.EvaluationsSummaryDTO;

import java.io.Serializable;
import java.util.List;

public class PlaceDTO implements Serializable {
    private Long placeId;
    private String name;
    private String description;
    private CategoryDTO category;
    private String image;
    private AddressDTO address;
    private String websiteLink;
    private boolean active;
    private Long accountId;
    private List<EvaluationDTO> evaluations;
    private EvaluationsSummaryDTO evaluationsSummary;

    public Long getPlaceId() {
        return placeId;
    }

    public void setPlaceId(Long placeId) {
        this.placeId = placeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public CategoryDTO getCategory() {
        return category;
    }

    public void setCategory(CategoryDTO category) {
        this.category = category;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public AddressDTO getAddress() {
        return address;
    }

    public void setAddress(AddressDTO address) {
        this.address = address;
    }

    public String getWebsiteLink() {
        return websiteLink;
    }

    public void setWebsiteLink(String websiteLink) {
        this.websiteLink = websiteLink;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public List<EvaluationDTO> getEvaluations() {
        return evaluations;
    }

    public void setEvaluations(List<EvaluationDTO> evaluations) {
        this.evaluations = evaluations;
    }

    public EvaluationsSummaryDTO getEvaluationsSummary() {
        return evaluationsSummary;
    }

    public void setEvaluationsSummary(EvaluationsSummaryDTO evaluationsSummary) {
        this.evaluationsSummary = evaluationsSummary;
    }
}
