package pl.touristguide.springapp.place;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import pl.touristguide.model.Account;
import pl.touristguide.model.Category;
import pl.touristguide.model.Place;
import pl.touristguide.springapp.place.dto.search.OrderField;
import pl.touristguide.springapp.place.dto.search.Ownership;
import pl.touristguide.springapp.place.dto.search.PlaceSearchParamsDTO;

import javax.persistence.EntityManager;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class PlaceRepositoryImpl implements PlaceRepository {
    private final EntityManager entityManager;

    @Autowired
    public PlaceRepositoryImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public List<Place> searchPlaces(PlaceSearchParamsDTO searchParams, Long accountId) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Place> criteriaQuery = criteriaBuilder.createQuery(Place.class);

        Root<Place> place = criteriaQuery.from(Place.class);
        List<Predicate> predicates = new ArrayList<>();

        if(searchParams.getName() != null && !searchParams.getName().isEmpty()) {
            predicates.add(criteriaBuilder.like(place.get("name"), "%" + searchParams.getName() + "%"));
        }
        if(searchParams.getCategoryId() != null) {
            Join<Category, Place> joinCategory = place.join("category");
            predicates.add(criteriaBuilder.equal(joinCategory.get("categoryId"), searchParams.getCategoryId()));
        }
        if(searchParams.getOwnership() == Ownership.MY) {
            Join<Account, Place> joinAccount = place.join("account");
            predicates.add(criteriaBuilder.equal(joinAccount.get("accountId"), accountId));
        }

        criteriaQuery.where(predicates.toArray(new Predicate[0]));

        if(searchParams.getOrderField() == OrderField.NAME_ASC) {
            criteriaQuery.orderBy(criteriaBuilder.asc(place.get("name")));
        }
        else if(searchParams.getOrderField() == OrderField.NAME_DESC) {
            criteriaQuery.orderBy(criteriaBuilder.desc(place.get("name")));
        }

        return entityManager.createQuery(criteriaQuery).getResultList();
    }
}
