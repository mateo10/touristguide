package pl.touristguide.springapp.place.address;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.touristguide.model.Address;

import java.util.List;

@Service
public class AddressFacade {
    private final AddressService addressService;

    @Autowired
    public AddressFacade(AddressService addressService) {
        this.addressService = addressService;
    }

    public Address insertAddress(Address address) {
        return addressService.insertAddress(address);
    }

    public List<Address> insertAddresses(List<Address> addresses) {
        return addressService.insertAddresses(addresses);
    }
}
