package pl.touristguide.springapp.place.dto.search;

public enum Ownership {
    MY, ALL
}
