package pl.touristguide.springapp.place.dto.search;

public enum OrderField {
    NAME_ASC, NAME_DESC, RATE_ASC, RATE_DESC
}
