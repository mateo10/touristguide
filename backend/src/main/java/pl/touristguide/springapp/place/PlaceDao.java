package pl.touristguide.springapp.place;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.touristguide.model.Place;

@Repository
interface PlaceDao extends CrudRepository<Place, Long> {
}
