package pl.touristguide.springapp.place.evaluation;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.touristguide.model.PlaceEvaluation;

@Repository
public interface PlaceEvaluationDao extends CrudRepository<PlaceEvaluation, Long> {
}
