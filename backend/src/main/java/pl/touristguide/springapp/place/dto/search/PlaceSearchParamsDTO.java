package pl.touristguide.springapp.place.dto.search;

import pl.touristguide.springapp.place.dto.PlaceDTO;

import java.util.List;

public class PlaceSearchParamsDTO {
    private String name;
    private Integer categoryId;
    private Ownership ownership;
    private OrderField orderField;
    private List<PlaceDTO> rejectedPlaces;

    public String getName() {
        return name;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public Ownership getOwnership() {
        return ownership;
    }

    public OrderField getOrderField() {
        return orderField;
    }

    public List<PlaceDTO> getRejectedPlaces() {
        return rejectedPlaces;
    }
}
