package pl.touristguide.springapp.place;

import pl.touristguide.model.Place;
import pl.touristguide.springapp.place.dto.search.PlaceSearchParamsDTO;

import java.util.List;

public interface PlaceRepository {
    List<Place> searchPlaces(PlaceSearchParamsDTO searchParams, Long accountId);
}
