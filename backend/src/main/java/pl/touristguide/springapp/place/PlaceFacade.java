package pl.touristguide.springapp.place;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.touristguide.model.Place;
import pl.touristguide.springapp.evaluation.dto.EvaluationDTO;
import pl.touristguide.springapp.place.dto.PlaceDTO;
import pl.touristguide.springapp.place.dto.PlaceEvaluationDTO;
import pl.touristguide.springapp.place.dto.search.PlaceSearchParamsDTO;
import pl.touristguide.springapp.place.evaluation.PlaceEvaluationService;

import java.util.List;

@Service
public class PlaceFacade {
    private final PlaceService placeService;
    private final PlaceEvaluationService placeEvaluationService;

    @Autowired
    public PlaceFacade(PlaceService placeService, PlaceEvaluationService placeEvaluationService) {
        this.placeService = placeService;
        this.placeEvaluationService = placeEvaluationService;
    }

    public List<PlaceDTO> getPlaces() throws Exception {
        return placeService.getPlaces();
    }

    public PlaceDTO getPlace(Long placeId) {
        return placeService.getPlace(placeId);
    }

    public PlaceDTO insertPlace(PlaceDTO placeDTO) throws Exception {
        return placeService.insertPlace(placeDTO);
    }

    public PlaceDTO updatePlace(Long placeId, PlaceDTO placeDTO) throws Exception {
        return placeService.updatePlace(placeId, placeDTO);
    }

    public void deactivatePlace(Long placeId) throws Exception {
        placeService.deactivatePlace(placeId);
    }

    public List<PlaceDTO> searchPlaces(PlaceSearchParamsDTO searchParams) throws Exception {
        return placeService.searchPlaces(searchParams);
    }

    public List<PlaceDTO> searchPlacesByCategoryId(Long categoryId) throws Exception {
        return placeService.searchPlacesByCategoryId(categoryId);
    }

    public List<PlaceDTO> searchPlacesByAccountId(Long accountId) throws Exception {
        return placeService.searchPlacesByAccountId(accountId);
    }

    public void insertPlaces(List<Place> places) {
        placeService.insertPlaces(places);
    }

    public EvaluationDTO insertPlaceEvaluation(PlaceEvaluationDTO placeEvaluationDTO) throws Exception {
        Place place = placeService.getPlaceModel(placeEvaluationDTO.getPlaceId());
        return placeEvaluationService.insertPlaceEvaluation(placeEvaluationDTO, place);
    }
}
