package pl.touristguide.springapp.place;

import pl.touristguide.model.Address;
import pl.touristguide.model.Place;
import pl.touristguide.springapp.category.CategoryMapper;
import pl.touristguide.springapp.evaluation.EvaluationMapper;
import pl.touristguide.springapp.place.dto.AddressDTO;
import pl.touristguide.springapp.place.dto.LocationDTO;
import pl.touristguide.springapp.place.dto.PlaceDTO;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.stream.Collectors;

public class PlaceMapper {

    public static List<Place> toPlaceList(Queue<PlaceDTO> placeDTOQueue){
        return placeDTOQueue.stream()
                .map(tmpPlace -> toPlace(tmpPlace))
                .collect(Collectors.toList());
    }

    public static Place toPlace(PlaceDTO placeDTO) {
        Place place = new Place();
        place.setPlaceId(placeDTO.getPlaceId());
        place.setName(placeDTO.getName());
        place.setDescription(placeDTO.getDescription());
        place.setWebsiteLink(placeDTO.getWebsiteLink());
        place.setActive(placeDTO.isActive());
        place.setCategory(CategoryMapper.toCategory(placeDTO.getCategory()));
        return place;
    }

    public static Queue<PlaceDTO> toPlaceDTOQueue(List<Place> places) {
        Queue<PlaceDTO> placeDTOS = new LinkedList<>();
        for(Place tmpPlace : places)
            placeDTOS.offer(toPlaceDTO(tmpPlace));
        return placeDTOS;
    }

    public static PlaceDTO toPlaceDTO(Place place) {
        PlaceDTO placeDTO = new PlaceDTO();
        placeDTO.setPlaceId(place.getPlaceId());
        placeDTO.setName(place.getName());
        placeDTO.setDescription(place.getDescription());
        placeDTO.setImage(place.getImageName());
        placeDTO.setAddress(toAddressDTO(place.getAddress()));
        placeDTO.setWebsiteLink(place.getWebsiteLink());
        placeDTO.setActive(place.isActive());
        placeDTO.setCategory(CategoryMapper.toCategoryDTO(place.getCategory()));
        placeDTO.setAccountId(place.getAccount() != null && place.getAccount().getAccountId() != null ? place.getAccount().getAccountId() : null);
        if(place.getEvaluations() != null) {
            placeDTO.setEvaluations(place.getEvaluations().stream()
                    .map(EvaluationMapper::placeEvaluationToEvaluationDTO)
                    .collect(Collectors.toList()));
        }
        return placeDTO;
    }

    public static Address toAddress(AddressDTO addressDTO) {
        Address address = new Address();
        address.setAddressId(addressDTO.getAddressId());
        address.setLatitude(addressDTO.getLocation().getLat());
        address.setLongitude(addressDTO.getLocation().getLng());
        address.setBuildingNr(addressDTO.getBuildingNr());
        address.setStreet(addressDTO.getStreet());
        address.setCity(addressDTO.getCity());
        address.setPostalCode(addressDTO.getPostalCode());
        address.setCounty(addressDTO.getCounty());
        return address;
    }

    private static AddressDTO toAddressDTO(Address address) {
        AddressDTO addressDTO = new AddressDTO();
        addressDTO.setAddressId(address.getAddressId());
        addressDTO.setLocation(toLocationDTO(address.getLatitude(), address.getLongitude()));
        addressDTO.setBuildingNr(address.getBuildingNr());
        addressDTO.setStreet(address.getStreet());
        addressDTO.setCity(address.getCity());
        addressDTO.setPostalCode(address.getPostalCode());
        addressDTO.setCounty(address.getCounty());
        return addressDTO;
    }

    private static LocationDTO toLocationDTO(BigDecimal lat, BigDecimal lng) {
        LocationDTO locationDTO = new LocationDTO();
        locationDTO.setLat(lat);
        locationDTO.setLng(lng);
        return locationDTO;
    }
}
