package pl.touristguide.springapp.place.evaluation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.touristguide.model.Account;
import pl.touristguide.model.Place;
import pl.touristguide.model.PlaceEvaluation;
import pl.touristguide.springapp.evaluation.EvaluationMapper;
import pl.touristguide.springapp.evaluation.dto.EvaluationDTO;
import pl.touristguide.springapp.place.dto.PlaceEvaluationDTO;
import pl.touristguide.springapp.user.UserFacade;

@Service
public class PlaceEvaluationService {
    private final PlaceEvaluationDao placeEvaluationDao;
    private final UserFacade userFacade;

    @Autowired
    public PlaceEvaluationService(PlaceEvaluationDao placeEvaluationDao, UserFacade userFacade) {
        this.placeEvaluationDao = placeEvaluationDao;
        this.userFacade = userFacade;
    }

    public EvaluationDTO insertPlaceEvaluation(PlaceEvaluationDTO placeEvaluationDTO, Place place) throws Exception {
        Account account = userFacade.findAccountById(placeEvaluationDTO.getAccountId());
        PlaceEvaluation placeEvaluation = PlaceEvaluationMapper.toPlaceEvaluation(placeEvaluationDTO, place, account);
        return EvaluationMapper.placeEvaluationToEvaluationDTO(placeEvaluationDao.save(placeEvaluation));
    }
}
