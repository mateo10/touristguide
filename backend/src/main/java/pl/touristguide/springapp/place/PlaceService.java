package pl.touristguide.springapp.place;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.touristguide.common.ImageUtils;
import pl.touristguide.model.Account;
import pl.touristguide.model.Place;
import pl.touristguide.springapp.category.CategoryFacade;
import pl.touristguide.springapp.category.CategoryMapper;
import pl.touristguide.springapp.evaluation.EvaluationSummaryService;
import pl.touristguide.springapp.evaluation.dto.EvaluationsSummaryDTO;
import pl.touristguide.springapp.place.address.AddressFacade;
import pl.touristguide.springapp.place.dto.PlaceDTO;
import pl.touristguide.springapp.place.dto.search.OrderField;
import pl.touristguide.springapp.place.dto.search.Ownership;
import pl.touristguide.springapp.place.dto.search.PlaceSearchParamsDTO;
import pl.touristguide.springapp.user.UserFacade;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
class PlaceService {
    private final PlaceDao placeDao;
    private final PlaceRepository placeRepository;
    private final CategoryFacade categoryFacade;
    private final UserFacade userFacade;
    private final EvaluationSummaryService evaluationSummaryService;
    private final AddressFacade addressFacade;

    @Autowired
    PlaceService(PlaceDao placeDao, PlaceRepository placeRepository, CategoryFacade categoryFacade, UserFacade userFacade,
                 EvaluationSummaryService evaluationSummaryService, AddressFacade addressFacade) {
        this.placeDao = placeDao;
        this.placeRepository = placeRepository;
        this.categoryFacade = categoryFacade;
        this.userFacade = userFacade;
        this.evaluationSummaryService = evaluationSummaryService;
        this.addressFacade = addressFacade;
    }

    List<PlaceDTO> getPlaces() {
        Iterable<Place> foundPlaces = this.placeDao.findAll();

        return StreamSupport.stream(foundPlaces.spliterator(), false)
                .map(this::buildPlaceDTO)
                .collect(Collectors.toList());
    }

    PlaceDTO getPlace(Long placeId) {
        Place place = this.placeDao.findById(placeId).orElse(null);
        assert place != null;
        return buildPlaceDTO(place);
    }

    @Transactional
    PlaceDTO insertPlace(PlaceDTO placeDTO) throws Exception {
        Place place = PlaceMapper.toPlace(placeDTO);
        Account account = userFacade.findAccountById(placeDTO.getAccountId());
        if(placeDTO.getImage() != null && !placeDTO.getImage().equals(""))
            place.setImageName(ImageUtils.createImage(placeDTO.getImage(), placeDTO.getName()));
        place.setAccount(account);
        place.setCategory(this.categoryFacade.getCategory(placeDTO.getCategory().getCategoryId()));
        place.setActive(true);
        place.setAddress(addressFacade.insertAddress(PlaceMapper.toAddress(placeDTO.getAddress())));
        return buildPlaceDTO(this.placeDao.save(place));
    }

    PlaceDTO updatePlace(Long placeId, PlaceDTO placeDTO) {
        Place place = this.placeDao.findById(placeId).orElse(null);
        assert place != null;
        rebuildPlace(place, placeDTO);
        return PlaceMapper.toPlaceDTO(this.placeDao.save(place));
    }

    void deletePlace(Long placeId) {
//        PlaceDTO place = getPlace(placeId);
        //ImageUtils.deleteImage(place.getImage());
        this.placeDao.deleteById(placeId);
    }

    void deactivatePlace(Long placeId) {
//        placeDao.deactivatePlace(placeId);
    }

    List<PlaceDTO> searchPlaces(PlaceSearchParamsDTO searchParams) {
        Long currentAccountId = null;
        if(searchParams.getOwnership() == Ownership.MY) {
            currentAccountId = userFacade.findAccountDetails().getAccount().getAccountId();
        }
        List<Place> places = placeRepository.searchPlaces(searchParams, currentAccountId);

        return places.stream()
                .map(this::buildPlaceDTO)
                .filter(place -> rejectPlaces(place, searchParams.getRejectedPlaces()))
                .sorted(getPlacesRatingComparator(searchParams))
                .collect(Collectors.toList());
    }

    List<PlaceDTO> searchPlacesByCategoryId(Long categoryId) throws Exception {
        return this.categoryFacade.getCategory(categoryId).getPlaces()
                .stream()
                .map(this::buildPlaceDTO)
                .collect(Collectors.toList());
    }

    List<PlaceDTO> searchPlacesByAccountId(Long accountId) throws Exception {
        return this.userFacade.findAccountById(accountId).getPlaces()
                .stream()
                .map(this::buildPlaceDTO)
                .collect(Collectors.toList());
    }

    void insertPlaces(List<Place> places) {
        placeDao.saveAll(places);
    }

    Place getPlaceModel(Long placeId) {
        return this.placeDao.findById(placeId).orElse(null);
    }

    private boolean selectMatchingPlaces(PlaceDTO placePattern, PlaceDTO placeDTO) {
        boolean isFit = false;

        if(placePattern.getCategory() != null && placeDTO.getCategory().getName().equals(placePattern.getCategory().getName()))
            isFit =  true;
        if(placePattern.getName() != null && !placePattern.getName().equals("") && placeDTO.getName().matches(placePattern.getName()))
            isFit =  true;

        return isFit;
    }

    private void rebuildPlace(Place place, PlaceDTO placeDTO) {
        place.setName(placeDTO.getName());
        place.setDescription(placeDTO.getDescription());
        place.getAddress().setLatitude(placeDTO.getAddress().getLocation().getLat());
        place.getAddress().setLongitude(placeDTO.getAddress().getLocation().getLng());
        place.setWebsiteLink(placeDTO.getWebsiteLink());
        place.setCategory(CategoryMapper.toCategory(placeDTO.getCategory()));
        place.setImageName(placeDTO.getImage());
    }

    private PlaceDTO buildPlaceDTO(Place place) {
        PlaceDTO placeDTO = PlaceMapper.toPlaceDTO(place);
        placeDTO.setEvaluationsSummary(evaluationSummaryService.summarizeEvaluations(placeDTO.getEvaluations()));
        return placeDTO;
    }

    private Comparator<PlaceDTO> getPlacesRatingComparator(PlaceSearchParamsDTO searchParams) {
        if(searchParams.getOrderField() == OrderField.RATE_ASC) {
            return Comparator.comparing(PlaceDTO::getEvaluationsSummary, getAverageRatingComparator());
        }
        else if(searchParams.getOrderField() == OrderField.RATE_DESC) {
            return Comparator.comparing(PlaceDTO::getEvaluationsSummary, getAverageRatingComparator()).reversed();
        }
        else {
            return (a1, a2) -> 0;
        }
    }

    private Comparator<EvaluationsSummaryDTO> getAverageRatingComparator() {
        return Comparator.comparingDouble(EvaluationsSummaryDTO::getAverageRating);
    }

    private boolean rejectPlaces(PlaceDTO place, List<PlaceDTO> rejectedPlaces) {
        if(rejectedPlaces == null || rejectedPlaces.size() == 0) {
            return true;
        }

        return !rejectedPlaces.stream()
                .anyMatch(rejectedPlace -> rejectedPlace.getPlaceId() == place.getPlaceId());
    }
}
