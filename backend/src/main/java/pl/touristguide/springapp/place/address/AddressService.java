package pl.touristguide.springapp.place.address;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.touristguide.model.Address;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
class AddressService {
    private final AddressDao addressDao;

    @Autowired
    public AddressService(AddressDao addressDao) {
        this.addressDao = addressDao;
    }


    Address insertAddress(Address address) {
        return addressDao.save(address);
    }

    List<Address> insertAddresses(List<Address> addresses) {
        Iterable<Address> insertedAddresses = addressDao.saveAll(addresses);
        return StreamSupport.stream(insertedAddresses.spliterator(), false)
                .collect(Collectors.toList());
    }
}
