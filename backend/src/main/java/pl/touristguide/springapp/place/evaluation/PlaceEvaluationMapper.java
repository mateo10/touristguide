package pl.touristguide.springapp.place.evaluation;

import pl.touristguide.model.Account;
import pl.touristguide.model.Place;
import pl.touristguide.model.PlaceEvaluation;
import pl.touristguide.springapp.place.dto.PlaceEvaluationDTO;

import java.time.LocalDateTime;

public class PlaceEvaluationMapper {

    public static PlaceEvaluation toPlaceEvaluation(PlaceEvaluationDTO placeEvaluationDTO,
                                                    Place place, Account account) {
        PlaceEvaluation placeEvaluation = new PlaceEvaluation();
        placeEvaluation.setCommentary(placeEvaluationDTO.getComment());
        placeEvaluation.setRate(placeEvaluationDTO.getRate());
        placeEvaluation.setInsertionDate(LocalDateTime.now());
        placeEvaluation.setPlace(place);
        placeEvaluation.setAccount(account);
        return placeEvaluation;
    }
}
