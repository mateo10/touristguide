package pl.touristguide.springapp.evaluation;

import pl.touristguide.model.PlaceEvaluation;
import pl.touristguide.model.RouteEvaluation;
import pl.touristguide.springapp.evaluation.dto.EvaluationDTO;
import pl.touristguide.springapp.user.UserMapper;

public class EvaluationMapper {
    public static EvaluationDTO placeEvaluationToEvaluationDTO(PlaceEvaluation evaluation) {
        EvaluationDTO evaluationDTO = new EvaluationDTO();
        evaluationDTO.setId(evaluation.getPlaceEvaluationId());
        evaluationDTO.setComment(evaluation.getCommentary());
        evaluationDTO.setRate(evaluation.getRate());
        evaluationDTO.setInsertionDate(evaluation.getInsertionDate());
        evaluationDTO.setAccount(UserMapper.toAccountDTO(evaluation.getAccount()));
        return evaluationDTO;
    }
    
    public static EvaluationDTO routeEvaluationToEvaluationDTO(RouteEvaluation evaluation) {
        EvaluationDTO evaluationDTO = new EvaluationDTO();
        evaluationDTO.setId(evaluation.getRouteEvaluationId());
        evaluationDTO.setComment(evaluation.getCommentary());
        evaluationDTO.setRate(evaluation.getRate());
        evaluationDTO.setInsertionDate(evaluation.getInsertionDate());
        evaluationDTO.setAccount(UserMapper.toAccountDTO(evaluation.getAccount()));
        return evaluationDTO;
    }
}
