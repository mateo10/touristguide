package pl.touristguide.springapp.evaluation.dto;

import pl.touristguide.springapp.user.dto.AccountDTO;

import java.io.Serializable;
import java.time.LocalDateTime;

public class EvaluationDTO implements Serializable {
    private Long id;
    private String comment;
    private Integer rate;
    private LocalDateTime insertionDate;
    private AccountDTO account;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Integer getRate() {
        return rate;
    }

    public LocalDateTime getInsertionDate() {
        return insertionDate;
    }

    public void setInsertionDate(LocalDateTime insertionDate) {
        this.insertionDate = insertionDate;
    }

    public void setRate(Integer rate) {
        this.rate = rate;
    }

    public AccountDTO getAccount() {
        return account;
    }

    public void setAccount(AccountDTO account) {
        this.account = account;
    }
}
