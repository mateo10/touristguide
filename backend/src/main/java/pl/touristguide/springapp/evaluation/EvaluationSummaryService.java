package pl.touristguide.springapp.evaluation;

import org.springframework.stereotype.Service;
import pl.touristguide.springapp.evaluation.dto.EvaluationDTO;
import pl.touristguide.springapp.evaluation.dto.EvaluationsSummaryDTO;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.averagingInt;
import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;

@Service
public class EvaluationSummaryService {
    public EvaluationsSummaryDTO summarizeEvaluations(List<EvaluationDTO> evaluations) {
        EvaluationsSummaryDTO  summary = new EvaluationsSummaryDTO();
        summary.setAverageRating(getAverage(evaluations));
        summary.setEvaluationsQuantity(getQuantity(evaluations));
        Map<Integer, Long> starsCount = getStarsQuantity(evaluations);
        summary.setFiveStars(starsCount.containsKey(5) ? Math.toIntExact(starsCount.get(5)) : 0);
        summary.setFourStars(starsCount.containsKey(4) ? Math.toIntExact(starsCount.get(4)) : 0);
        summary.setThreeStars(starsCount.containsKey(3) ? Math.toIntExact(starsCount.get(3)) : 0);
        summary.setTwoStars(starsCount.containsKey(2) ? Math.toIntExact(starsCount.get(2)) : 0);
        summary.setOneStar(starsCount.containsKey(1) ? Math.toIntExact(starsCount.get(1)) : 0);
        return summary;
    }

    private double getAverage(List<EvaluationDTO> evaluations) {
        if(evaluations == null || evaluations.size() == 0) {
            return 0;
        }

        return evaluations.stream()
                .collect(averagingInt(EvaluationDTO::getRate));
    }

    private Integer getQuantity(List<EvaluationDTO> evaluations) {
        if(evaluations == null) {
            return 0;
        }

        return evaluations.size();
    }

    private Map<Integer, Long> getStarsQuantity(List<EvaluationDTO> evaluations) {
        if(evaluations == null || evaluations.size() == 0) {
            return new HashMap<>();
        }

        return evaluations.stream()
                .collect(groupingBy(EvaluationDTO::getRate, counting()));
    }
}
