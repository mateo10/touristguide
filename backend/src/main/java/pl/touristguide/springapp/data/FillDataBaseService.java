package pl.touristguide.springapp.data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import pl.touristguide.common.HashUtils;
import pl.touristguide.model.*;
import pl.touristguide.springapp.category.CategoryFacade;
import pl.touristguide.springapp.place.PlaceFacade;
import pl.touristguide.springapp.place.address.AddressFacade;
import pl.touristguide.springapp.user.UserFacade;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Component
public class FillDataBaseService implements ApplicationRunner {
    private final CategoryFacade categoryFacade;
    private final PlaceFacade placeFacade;
    private final UserFacade userFacade;
    private final AddressFacade addressFacade;

    @Value("${database.load-data}")
    private Boolean loadData;

    @Autowired
    public FillDataBaseService(CategoryFacade categoryFacade, PlaceFacade placeFacade, UserFacade userFacade,
                               AddressFacade addressFacade) {
        this.categoryFacade = categoryFacade;
        this.placeFacade = placeFacade;
        this.userFacade = userFacade;
        this.addressFacade = addressFacade;
    }

    public void run(ApplicationArguments args) throws Exception {
        if(loadData) {
            loadData();
        }
    }

    private void loadData() throws Exception {
        List<Category> categories = categoryFacade.insertCategories(loadCategories());
        UserDetail userDetail = this.userFacade.insertUserDetail(loadUserDetail());
        Account account = userFacade.insertAccount(loadAccount(userDetail));
        List<Address> addresses = addressFacade.insertAddresses(loadAddresses());
        List<Place> places = loadPlaces(categories, addresses, account);
        this.placeFacade.insertPlaces(places);
    }

    private List<Category> loadCategories() {
        List<Category> categories = new ArrayList<>();
        categories.add(addCategory("ZABYTKI"));
        categories.add(addCategory("MUZEA"));
        categories.add(addCategory("TRANSPORT"));
        categories.add(addCategory("ROZRYWKA"));
        categories.add(addCategory("NOCLEG"));
        categories.add(addCategory("PRZYRODA"));
        categories.add(addCategory("RESTAURACJE"));
        categories.add(addCategory("SPORT I REKREACJA"));
        categories.add(addCategory("NOCNE ŻYCIE"));
        categories.add(addCategory("WYDARZENIA"));
        categories.add(addCategory("ZAKUPY"));
        categories.add(addCategory("BEZPIECZEŃSTWO"));
        return categories;
    }

    private List<Address> loadAddresses() {
        List<Address> addresses = new ArrayList<>();
        addresses.add(addAddress(new BigDecimal(50.8660253818053), new BigDecimal(20.6285212025977),
                "5", "Zamkowa", "Kielce", "25-009", "Kielce"));

        addresses.add(addAddress(new BigDecimal(50.8807268310492), new BigDecimal(20.6465721130371),
                "20", "Świętokrzyska", "Kielce", "25-406", "Kielce"));

        addresses.add(addAddress(new BigDecimal(50.8985400058829), new BigDecimal(20.5866622924805),
                "1", "Zakładowa", "Kielce", "25-672", "Kielce"));

        addresses.add(addAddress(new BigDecimal(50.875556), new BigDecimal(20.635556),
                "26", "Warszawska", "Kielce", "25-312", "Kielce"));

        addresses.add(addAddress(new BigDecimal(50.859710), new BigDecimal(20.618582),
                "1", "Aleja Legionów", "Kielce", "25-001", "Kielce"));

        addresses.add(addAddress(new BigDecimal(50.861167), new BigDecimal(20.616944),
                "1", "Aleja Legionów", "Kielce", "25-001", "Kielce"));

        addresses.add(addAddress(new BigDecimal(50.855886), new BigDecimal(20.610575),
                "2", "Leszka Drogosza", "Kielce", "25-093", "Kielce"));

        addresses.add(addAddress(new BigDecimal(50.855556), new BigDecimal(20.613611),
                "1", "Leszka Drogosza", "Kielce", "25-093", "Kielce"));

        addresses.add(addAddress(new BigDecimal(50.868292), new BigDecimal(20.634825),
                "2", "Wolności", "Kielce", "25-367", "Kielce"));

        addresses.add(addAddress(new BigDecimal(50.875753), new BigDecimal(20.621725),
                "12", "Czarnowska ", "Kielce", "25-504", "Kielce"));

        addresses.add(addAddress(new BigDecimal(50.861389), new BigDecimal(20.624722),
                "8", "Ściegiennego ", "Kielce", "25-033", "Kielce"));

        addresses.add(addAddress(new BigDecimal(50.987267), new BigDecimal(20.650128),
                "115", "Turystyczna", "Zagnańsk", "26-050", "Kielce"));

        addresses.add(addAddress(new BigDecimal(51.047972), new BigDecimal(21.067139),
                "1", "Wielkopiecowa ", "Starachowice", "27-200", "starachowicki"));

        addresses.add(addAddress(new BigDecimal(51.0175), new BigDecimal(21.548611),
                "8a", "Bałtów", "Bałtów", "27-423", "ostrowiecki"));

        addresses.add(addAddress(new BigDecimal(50.797222), new BigDecimal(20.460278),
                "7", "Małogoska", "Chęciny", "26-060", "kielecki"));

        addresses.add(addAddress(new BigDecimal(50.82289), new BigDecimal(20.50337),
                "", "Dobrzączka", "Chęciny", "26-060", "kielecki"));

        addresses.add(addAddress(new BigDecimal(50.763919), new BigDecimal(20.435064),
                "303", "Tokarnia", "Tokarnia", "26-060", "Kielce"));
        return addresses;
    }

    private List<Place> loadPlaces(List<Category> categories, List<Address> addresses, Account account) {
        List<Place> places = new ArrayList<>();
        places.add(addPlace("Pałacyk Zielińskiego", "Pałacyk Zielińskiego.png",
                "Jeden z najcenniejszych zabytków architektury Kielc, znajduje się w centrum miasta przy ulicy Zamkowej, granicząc z innymi zabytkami: Parkiem Miejskim i byłym więzieniem kieleckim. Ulica Zamkowa ciągnęła się dawniej aż do ulicy Chęcińskiej. Dalej w stronę Krakowa wiódł tędy stary trakt zwany 'Starą Drogą'. Zabudowę ulicy stanowiły budynki przypałacowe zaczynające się od południowego skrzydła pałacu i XVII-wiecznej bramy aż do stawu. Były tu stajnie, wozownie, budynek starosty, spichlerz, mieszkanie dla pisarza prewentowego i dla praczki oraz tzw. rajszula, czyli ujeżdżalnia koni (szkoła końska) wybudowana w 1752 r. przez biskupa Andrzeja Załuskiego. To właśnie na jej fundamentach wzniesiono w latach 1847-1857 zabudowania pałacyku Tomasza Zielińskiego.",
                categories.get(3), "http://www.palacykzielinskiego.pl", account, addresses.get(0)));

        places.add(addPlace("Galeria Echo", "Galeria Echo.png",
                "Największa galeria handlowa w Polsce pod względem liczby sklepów. Została otwarta 30 listopada 2002 roku. W latach 2009–2011 dokonano przebudowy według projektu pracowni architektonicznej Detan z Kielc, natomiast projekt elewacji i wnętrz wykonała warszawska pracownia architektoniczna Open Architekci. W 2011 roku Galeria Echo zwyciężyła w kategorii Best enlarged retail development (dla najlepszego rozbudowanego obiektu handlowego) w międzynarodowym konkursie MAPIC Awards. W Galerii Echo znajduje się około 300 sklepów i punktów usługowych. Centrum składa się z czterech poziomów (-1, 0, 1 i 2) – na najwyższym poziomie znajdują się: kręgielnia, siłownia, fitness, bawialnia dla dzieci, centrum medyczne, salony Komfort. Powierzchnia całkowita to 159 tys. m², z czego 70 tys. m² stanowi powierzchnia najmu",
                categories.get(10), "http://www.galeriaecho.pl", account, addresses.get(1)));

        places.add(addPlace("Targi kielce", "Targi kielce.png",
                "Polski ośrodek wystawienniczy. Wicelider targów w Polsce, członek UFI – Światowego związku Przemysłu Targowego, CENTREX – Międzynarodowego Związku Statystyk Targowych i do stycznia 2012 roku Polskiej Izby Przemysłu Targowego. Druga, co do wielkości, własna powierzchnia wystawowa w kraju. Klimatyzowane pawilony wystawowe z pełną infrastrukturą techniczną, nowoczesne centrum konferencyjne. Jedyne targi ze specjalnym terenem pokazowym do dynamicznej prezentacji każdego rodzaju sprzętu. Organizator specjalistycznych targów branżowych i wystaw gospodarczych oraz towarzyszących im konferencji, seminariów i sympozjów.",
                categories.get(9), "http://www.targikielce.pl", account, addresses.get(2)));

        places.add(addPlace("Galeria Korona", "Galeria Korona.png",
                "Galeria handlowa w centrum Kielc, w trójkącie ulic Warszawskiej, Polnej i Radiowej. Jej otwarcie nastąpiło 16 maja 2012 roku. Otwarcie Galerii Korona nastąpiło 16 maja 2012 roku, wartość inwestycji wyniosła 100 mln euro. Powierzchnia całkowita liczy sobie 93,5 tys. m², natomiast powierzchnia najmu 36 tys. m². W Galerii znajduje się od 160 do 170 lokali handlowo-usługowych oraz punktów gastronomicznych, a także supermarket (delikatesy Piotr i Paweł) i Multikino z dziewięcioma salami na 1490 miejsc.",
                categories.get(10), "https://galeria-korona.pl/", account, addresses.get(3)));

        places.add(addPlace("Amfiteatr Kadzielnia", "Amfiteatr Kadzielnia.png",
                "Amfiteatr Kadzielnia znajduje się w południowej części wyrobiska Kadzielnia, po południowo-wschodniej stronie rezerwatu geologicznego o tej samej nazwie. Eksploatacja w kamieniołomie trwała do początku lat 60-tych, a historia wydobycia surowca na potrzeby przemysłu wapienniczego sięga przeszło 200 lat. Jednym z kluczowych etapów stopniowej rekultywacji i zagospodarowania dawnego kamieniołomu było utworzenie rezerwatu Kadzielnia oraz budowa amfiteatru. Obiekt oddano do użytku w 1971 roku, jako główny element projektu zagospodarowania przestrzennego dawnego wyrobiska wapieni dewońskich. W 2008 roku zainicjowano projekt przebudowy Amfiteatru Kadzielnia, który zakończył się ostatecznie w 2010 roku. Zmodernizowany Amfiteatr stał się nowoczesnym obiektem dostosowanym do potrzeb współczesnych widowisk artystycznych i festiwali.",
                categories.get(3), "http://amfiteatr-kadzielnia.pl/", account, addresses.get(4)));

        places.add(addPlace("Kadzielnia", "Kadzielnia.png",
                "Wzgórze, w południowo-zachodniej części Kielc. Kadzielnia, zbudowana z wapieni górnodewońskich (fran, famen), ma wysokość 295 m n.p.m. Obecne wyrobisko (wcześniej były tam kamieniołomy) wypełniają wody podziemne tworząc Jezioro Szmaragdowe. Kadzielnia stanowi największe skupisko jaskiń na Kielecczyźnie (jest ich tu 25), Najdłuższą jaskinią jest Jaskinia Odkrywców, obejmująca trzy połączone ze sobą jaskinie: Jaskinię Odkrywców, Prochownię i Szczelinę na Kadzielni. Nazwa Kadzielnia pochodzi od rosnącego tu jałowca wykorzystywanego do produkcji kadzideł, lub według innej wersji, od kadzielnika (kościelnego), który dzierżawił ten teren.",
                categories.get(5), "http://amfiteatr-kadzielnia.pl/", account, addresses.get(5)));

        places.add(addPlace("Hala Legionów", "Hala Legionów.png",
                "Hala Legionów w Kielcach to nowoczesny, wielofunkcyjny obiekt sportowy, w którym można rozgrywać mecze różnych dyscyplin sportowych. Hala znajduje się w południowej części miasta, przy ulicy Leszka Drogosza i położona jest 100 metrów od stadionu lekkoatletycznego \"Budowlani\" oraz 1000 metrów od miejskiego stadionu piłkarskiego. Tuż obok znajduje się kompleks leśny \"Stadion leśny\". Nazwa Hala Legionów została nadana obiektowi z okazji 50-lecia Świętokrzyskiego Związku Piłki Ręcznej. Rada Miasta Kielce uzasadniła jej wybór tym, że początki piłki ręcznej w województwie miały miejsce w tym samym czasie co odzyskanie niepodległości. Otwarcie hali odbyło się 25 sierpnia 2006 roku. Na znajdującej się w hali płycie z dębowym parkietem o wymiarach 55 na 38 metrów zmieszczą się pełnych wymiarów boiska do piłki ręcznej, futsalu, siatkówki, koszykówki lub trzy korty do tenisa.",
                categories.get(7), "", account, addresses.get(6)));

        places.add(addPlace("Stadion lekkoatletyczny", "Stadion lekkoatletyczny.png",
                "Stadion został wybudowany w 1989 roku na Ogólnopolską Spartakiadę Młodzieży[1]. W 1991 i 1993 roku odbyły się na nim mistrzostwa Polski seniorów w lekkoatletyce. Stadion lekkoatletyczny położony jest w obrębie Parku Kultury i Wypoczynku. Jest to ulubione miejsce do aktywnego wypoczynku mieszkańców Kielc. Na stokach góry Pierścienicy znajduje się orczykowy wyciąg narciarski. Dla miłośników spacerów i biegów jest nieskończona ilość ścieżek i duktów leśnych. Dla amatorów sportów rowerowych wokół góry Pierścienicy, wytyczono dwa szlaki rowerowe, a w pobliżu treningowego boiska sportowego urządzono rowerowy tor przeszkód. W parku znajdują się także: ścieżka zdrowia, siłownie na świeżym powietrzu, plac zabaw dla najmłodszych oraz odpłatny park linowy. Stadion L.A posiada nawierzchnię tartanową typu COMPUR SW.",
                categories.get(7), "http://www.mosir.kielce.pl/obiekty.php?oPath=3&obiekt_id=8", account, addresses.get(7)));

        places.add(addPlace("Muzeum Zabawek i Zabawy", "Muzeum Zabawek i Zabawy.png",
                "Muzeum Zabawek i Zabawy w Kielcach powstało w 1979 roku jako Muzeum Zabawkarstwa. Posiada ogromną kolekcję zabawek zarówno historycznych, jak i współczesnych. Jest to jedna z nielicznych i największa tego typu placówka w Polsce.",
                categories.get(1), "http://www.muzeumzabawek.eu/", account, addresses.get(8)));

        places.add(addPlace("Dworzec PKS", "Dworzec PKS.png",
                "Charakterystyczny budynek w kształcie rotundy znajdujący się w centrum Kielc przy ul. Czarnowskiej. Budowę rozpoczęto w 1975 roku, budynek oddano do użytku 20 lipca 1984, w czterdziestą rocznicę PRL. Zaprojektowali go arch. Edward Modrzejewski, inż. Jerzy Radkiewicz i inż. Mieczysław Kubala (odpowiedzialny za komunikację). Docelowo w założeniu miał obsługiwać 1500 autobusów i 24 000 pasażerów w ciągu doby. Budynek został wpisany na listę zabytków. Świętokrzyski konserwator zabytków oznajmił: „wzniesiony w latach 1975–1984 budynek dworcowy m.in. stanowi unikalny i nowatorski przykład architektury PRL, uznawany za jedną z cenniejszych realizacji architektonicznych w Polsce w latach 70. i 80. XX wieku, i materialny dokument epoki.",
                categories.get(2), "http://dworzec.kielce.pl/", account, addresses.get(9)));

        places.add(addPlace("Stadion Miejski", "Stadion Miejski.png",
                "stadion piłkarski znajdujący się w Kielcach przy ulicy księdza Piotra Ściegiennego 8. Jest to główny obiekt Korony Kielce. Stadion określany jest także nazwą Arena Kielc, Suzuki Arena (poprzednio Stadion Miejski, Kolporter Arena). Nowy stadion w Kielcach ma pojemność 15.550 miejsc siedzących usytuowanych na dwóch kondygnacjach. Wszystkie trybuny są zadaszone. Na stadionie znajduje się: boisko główne z podgrzewaną płytą o wymiarach 105x68 m, oświetlenie 1411 luksów (norma UEFA), pełne zaplecze dla kibiców, nowoczesne nagłośnienie, monitoring (49 kamer, boisko treningowe ze sztuczną nawierzchnią, boisko treningowe z nawierzchnią naturalną, odnowa biologiczna, siłownia, obok stadionu hotel, restauracja.",
                categories.get(7), "http://www.mosir.kielce.pl/obiekty.php?oPath=3&obiekt_id=4", account, addresses.get(10)));

        places.add(addPlace("Dąb Bartek", "Dąb Bartek.png",
                "jeden z najstarszych w Polsce dębów, od 1954 roku chroniony prawem jako pomnik przyrody, rosnący na terenie leśnictwa Bartków (nadleśnictwo Zagnańsk), przy drodze wojewódzkiej nr 750 z Zagnańska do Samsonowa, w województwie świętokrzyskim. W okresie międzywojennym wiek dębu oceniano nawet na 1200 lat[a]. Cezary Pacyniak w publikacji z 1992 roku Najstarsze drzewa w Polsce podał wyniki swoich badań, według których wiek Bartka wynosił wówczas 654 lata. Zgodnie z pomiarami wykonanymi w 2013 roku drzewo ma 28,5 metra wysokości",
                categories.get(5), "http://www.zagnansk.pl/", account, addresses.get(11)));

        places.add(addPlace("Muzeum Przyrody i Techniki Ekomuzeum im. Jana Pazdura", "Muzeum Przyrody i Techniki Ekomuzeum im. Jana Pazdura.png",
                "Placówka mieści się na terenie dawnej huty. Muzeum zostało utworzone w 2001 roku. Głównym orędownikiem jego powstania był prof. Jan Pazdur – historyk, dyrektor Wojewódzkiego Archiwum Państwowego w Kielcach oraz członek Instytutu Historii Kultury Materialnej Polskiej Akademii Nauk. Jest położone na 8 hektarach poindustrialnych terenów, na których umieszczono następujące ekspozycje stałe: \"Zakład wielkopiecowy z 1841 roku\", w skład której wchodzą zachowane budynki dawnej maszynowni oraz hali lejniczej, \"Wielki Piec z 1899 roku\" wraz z ciągiem technologicznym torowisk kolejowych oraz wieży gichtowej dostarczającej materiały do wytopu surówki, \"Górnictwo rud żelaza\", zorganizowana w budynku wieży ciśnień. Obejmuje ona eksponaty związane w wydobyciem rud żelaza na ziemi świętokrzyskiej, Technika samochodowa\", poświęcona historii Fabryki Samochodów Ciężarowych „Star”, \"Paleontologia\", obejmująca tropy dinozaurów ery mezozoicznej, \"Archeopark. Osada starożytnego hutnictwa żelaza\" - plenerowa ekspozycja, obejmująca rekonstrukcje osady z początku naszego tysiąclecia. Ponadto muzeum organizuje wystawy czasowe oraz imprezy kulturalne (koncerty).",
                categories.get(1), "http://ekomuzeum.pl/", account, addresses.get(12)));

        places.add(addPlace("Jura Park Bałtów", "Jura Park Bałtów.png",
                "kompleks turystyczny znajdujący się we wsi Bałtów w województwie świętokrzyskim, w powiecie ostrowieckim. Głównym elementem parku są rekonstrukcje dinozaurów naturalnej wielkości. Kompleks został otwarty 7 sierpnia 2004 r. Jego właścicielem jest Stowarzyszenie Delta. W skład kompleksu wchodzą: JuraPark Bałtów – blisko 100 modeli oryginalnej wielkości, Zwierzyniec Bałtowski – na obszarze 40 ha rozmieszczono wybiegi dla jeleni Dybowskiego, danieli, muflonów, bydła szkockiego itp., Spływ Kamienną – czynny od kwietnia do października, Park Rozrywki – karuzele, gokarty, kolejki dla młodszych i starszych dzieci, Stacja Narciarska Szwajcaria Bałtowska – ośrodek narciarski z wyciągiem krzesełkowym, dwoma wyciągami orczykowymi, dwoma wyciągami dla dzieci i sześcioma trasami o łącznej długości ok. 4 km. Stok czynny sezonowo. Wiosną 2013 r. rozpoczęła się trwająca do 2015 rewitalizacja zamku. Całkowity koszt inwestycji wynosił 8 143 446,69 zł.",
                categories.get(3), "https://juraparkbaltow.pl/", account, addresses.get(13)));

        places.add(addPlace("Zamek w Chęcinach", "Zamek w Chęcinach.png",
                "Zamek królewski z przełomu XIII i XIV wieku, górujący nad miejscowością Chęciny, w województwie świętokrzyskim, pod Kielcami. W pobliżu zamku przechodzi żółty szlak turystyczny im. Juliusza Brauna, Honorowego Członka PTTK, z Wiernej Rzeki do Chęcin. Zamek wzniesiony na splantowanej grzędzie, zbudowany z kamienia. Pierwotny zamek powstał na planie nieregularnego wieloboku wykorzystując walory obronne wzgórza wznoszącego się na wysokość 360 m n.p.m. W czasach funkcjonowania zamku, wzgórze zamkowe oraz pobliski teren były regularnie karczowane z wysokopiennej roślinności w tym głównie z drzew dla zachowania wysokiej widoczności i poprawy obronności. ",
                categories.get(1), "https://www.zamek.checiny.pl/pl/", account, addresses.get(14)));

        places.add(addPlace("Jaskinia Raj", "Jaskinia Raj.png",
                "Wapienna jaskinia krasowa położona w pobliżu Chęcin (Góry Świętokrzyskie) w województwie świętokrzyskim na terenie rezerwatu przyrody Jaskinia Raj, 11 km na południowy zachód od Kielc. Wyróżnia się wyjątkowo bogatą, różnorodną i dobrze zachowaną szatą naciekową; obok Jaskini Niedźwiedziej w Sudetach należy do unikatowych w Polsce obiektów krasowych. Jest udostępniona dla ruchu turystycznego. Jaskinia Raj znajduje się we wnętrzu niewielkiego wzniesienia Malik o wysokości około 270 m n.p.m. Jest to niewielka jaskinia o rozwinięciu poziomym, łączna długość jej korytarzy wynosi 240 metrów. ",
                categories.get(5), "http://jaskiniaraj.pl/pl/", account, addresses.get(15)));

        places.add(addPlace("Muzeum Wsi Kieleckiej", "Muzeum Wsi Kieleckiej.png",
                "Polskie muzeum etnograficzne z siedzibą w Kielcach. Celem działania muzeum jest gromadzenie, ochrona i udostępnianie zabytków kultury ludowej z obszaru województwa świętokrzyskiego, ze szczególnym uwzględnieniem budownictwa ludowego oraz popularyzacja kultury ludowej. Muzeum zostało powołane do życia decyzją wojewody kieleckiego z dn. 21 sierpnia 1976. Samodzielną działalność rozpoczęło 1 stycznia 1977. Głównym obiektem muzeum jest Park Etnograficzny (skansen) w Tokarni koło Chęcin, położony przy drodze krajowej nr 7 z Kielc do Krakowa. Zadaniem skansenu jest zachowanie zabytków budownictwa wiejskiego i małomiasteczkowego Kielecczyzny oraz prezentowanie ich w otoczeniu zbliżonym do pierwotnego i w naturalnych zespołach fragmentów wsi. ",
                categories.get(1), "http://mwk.com.pl/", account, addresses.get(16)));

        return places;
    }

    private UserDetail loadUserDetail() {
        return addUserDetail("admin", "admin@admin.com");
    }

    private Account loadAccount(UserDetail userDetail) throws Exception {
        return addAccount("admin", "admin", userDetail);
    }

    private Category addCategory(String name) {
        Category category = new Category();
        category.setName(name);
        category.setActive(true);
        return category;
    }

    private Place addPlace(String name, String image, String description, Category category,
                           String websiteLink, Account account, Address address) {
        Place place = new Place();
        place.setName(name);
        place.setDescription(description);
        place.setCategory(category);
        place.setImageName(image);
        place.setAddress(address);
        place.setWebsiteLink(websiteLink);
        place.setActive(true);
        place.setAccount(account);
        return place;
    }

    private Address addAddress(BigDecimal lat, BigDecimal lng, String buildingNr, String street, String city,
                               String postalCode, String county) {
        Address address = new Address();
        address.setLatitude(lat);
        address.setLongitude(lng);
        address.setBuildingNr(buildingNr);
        address.setStreet(street);
        address.setCity(city);
        address.setPostalCode(postalCode);
        address.setCounty(county);
        return address;
    }

    private UserDetail addUserDetail(String name, String email) {
        UserDetail userDetail = new UserDetail();
        userDetail.setName(name);
        userDetail.setEmail(email);
        return userDetail;
    }

    private Account addAccount(String login, String password, UserDetail userDetail) throws Exception {
        Account account = new Account();
        account.setUsername(login);
        account.setPassword(HashUtils.generateHash(password,  10));
        account.setEnabled(true);
        account.setRole("ADMIN");
        account.setUserDetail(userDetail);
        return account;
    }
}
