package pl.touristguide.springapp.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.touristguide.common.HashUtils;
import pl.touristguide.model.Account;
import pl.touristguide.model.UserDetail;
import pl.touristguide.springapp.user.dto.AccountDTO;
import pl.touristguide.springapp.user.dto.UserDetailDTO;

import java.util.Optional;

@Service
class UserService {
    private final AccountDao accountDao;
    private final UserDetailDao userDetailDao;

    @Autowired
    UserService(AccountDao accountDao, UserDetailDao userDetailDao) {
        this.accountDao = accountDao;
        this.userDetailDao = userDetailDao;
    }

    UserDetailDTO registration(UserDetailDTO userDetailDTO) throws Exception {
        validateAddAccount(userDetailDTO);
        UserDetail userDetail = userDetailDao.save(UserMapper.toUserDetail(userDetailDTO));
        AccountDTO accountDTO = userDetailDTO.getAccount();
        accountDTO.setRole("USER");
        accountDTO.setPassword(HashUtils.generateHash(accountDTO.getPassword(), 10));
        Account account = accountDao.save(UserMapper.toAccount(accountDTO, userDetail));
        return UserMapper.toUserDTO(account);
    }

    UserDetailDTO login(AccountDTO accountDTO) throws Exception {
        Optional<Account> optionalAccount = findAccountByLogin(accountDTO);
        Account modelAccount;

        if (optionalAccount.isPresent()) {
            modelAccount = optionalAccount.get();
        }
        else {
            throw new Exception("Niepoprawny login lub hasło.");
        }

        if (HashUtils.verifyPassword(accountDTO.getPassword(), modelAccount.getPassword())) {
            return UserMapper.toUserDTO(modelAccount);
        } else {
            throw new Exception("Niepoprawny login lub hasło.");
        }
    }

    void updateUserDetail(UserDetailDTO currentUser, UserDetailDTO userDetailDTO) throws Exception {
        if(!currentUser.getEmail().equals(userDetailDTO.getEmail())) {
            validateEmail(userDetailDTO);
        }
        userDetailDTO.setUserDetailId(currentUser.getUserDetailId());
        UserDetail userDetail = UserMapper.toUserDetail(userDetailDTO);
        this.userDetailDao.save(userDetail);
    }

    void deleteUserAccount(Long userDetailId) {
        userDetailDao.deleteById(userDetailId);
    }

    void deactivateAccount(Account currentAccount) {
        currentAccount.setEnabled(false);
        accountDao.save(currentAccount);
    }

    Account findAccountById(Long accountId) throws Exception {
        Optional<Account> account = accountDao.findById(accountId);
        if (account.get() == null) {
            throw new Exception(Account.class.getSimpleName());
        }

        return account.get();
    }

    UserDetail insertUserDetail(UserDetail userDetail) {
        return userDetailDao.save(userDetail);
    }

    Account insertAccount(Account account) {
        return accountDao.save(account);
    }

    void updateUserAccountPassword(Account currentAccount, AccountDTO accountDTO) throws Exception {
        validatePassword(currentAccount, accountDTO);
        currentAccount.setPassword(HashUtils.generateHash(accountDTO.getPassword(), 10));
        accountDao.save(currentAccount);
    }

    private void updateUserAccount(AccountDTO accountDTO, UserDetail userDetail) throws Exception {
        if(accountDTO.getCurrentPassword() != null && accountDTO.getPassword() != null) {
            accountDTO.setPassword(HashUtils.generateHash(accountDTO.getPassword(), 10));
            this.accountDao.save(UserMapper.toAccount(accountDTO, userDetail));
        }
    }

    private void validateAddAccount(UserDetailDTO userDetailDTO) throws Exception {
        if (findAccountByLogin(userDetailDTO.getAccount()).isPresent()) {
            throw new Exception("Konto o podanym loginie już istnieje.");
        }

        if (findUserDetailByEmail(userDetailDTO).isPresent()) {
            throw new Exception("Istnieje już konto przypisane do podanego adresu email.");
        }
    }

    private void validateLogin(UserDetailDTO userDetailDTO) throws Exception {
        Optional<Account> accountFoundedByLogin = findAccountByLogin(userDetailDTO.getAccount());
        if (accountFoundedByLogin.isPresent() && accountFoundedByLogin.get().getAccountId() != userDetailDTO.getAccount().getAccountId()) {
            throw new Exception("Konto o podanym loginie już istnieje.");
        }
    }

    private void validatePassword(Account currentAccount, AccountDTO account) throws Exception {
        if (!HashUtils.verifyPassword(account.getCurrentPassword(), currentAccount.getPassword())) {
            throw new Exception("Niepoprawe obecne hasło.");
        }
    }

    private void validateEmail(UserDetailDTO userDetailDTO) throws Exception {
        Optional<UserDetail> accountFoundedByEmail = findUserDetailByEmail(userDetailDTO);
        if (accountFoundedByEmail.isPresent() && accountFoundedByEmail.get().getUserDetailId() != userDetailDTO.getUserDetailId()) {
            throw new Exception("Istnieje już konto przypisane do podanego adresu email.");
        }
    }

    private Optional<Account> findAccountByLogin(AccountDTO accountDTO) throws Exception {
        return accountDao.findAccountByLogin(accountDTO.getLogin());
    }

    private Optional<UserDetail> findUserDetailByEmail(UserDetailDTO userDetailDTO) throws Exception {
        return userDetailDao.findUserDetailsByEmail(userDetailDTO.getEmail());
    }
}
