package pl.touristguide.springapp.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Service;
import pl.touristguide.model.Account;
import pl.touristguide.springapp.user.dto.AccountDTO;
import pl.touristguide.springapp.user.dto.UserDetailDTO;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static org.springframework.security.web.context.HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY;

@Service
public class AuthenticationService {
    private final AccountDao accountDao;
    private final AuthenticationManager authManager;

    @Autowired
    public AuthenticationService(AccountDao accountDao, AuthenticationManager authManager) {
        this.accountDao = accountDao;
        this.authManager = authManager;
    }


    UserDetailDTO login(AccountDTO accountDTO, HttpServletRequest request) {
        UsernamePasswordAuthenticationToken authReq
                = new UsernamePasswordAuthenticationToken(accountDTO.getLogin(), accountDTO.getPassword());
        Authentication auth = authManager.authenticate(authReq);

        SecurityContext sc = SecurityContextHolder.getContext();
        sc.setAuthentication(auth);
        HttpSession session = request.getSession(true);
        session.setAttribute(SPRING_SECURITY_CONTEXT_KEY, sc);
        return findAccountDetails();
    }

    UserDetailDTO findAccountDetails() {
        Account account = findCurrentAccount();
        return account != null ? UserMapper.toUserDTO(account) : null;
    }

    Account findCurrentAccount() {
        AccountDTO accountDTO = new AccountDTO();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (!(auth instanceof AnonymousAuthenticationToken)) {
            UserDetails userDetails = (UserDetails) auth.getPrincipal();
            accountDTO.setLogin(userDetails.getUsername());
        }

        return accountDao.findAccountByLogin(accountDTO.getLogin())
                .orElse(null);
    }

    void logoutUser(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        if (auth != null){
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
    }
}
