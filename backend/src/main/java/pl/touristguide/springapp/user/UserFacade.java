package pl.touristguide.springapp.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.touristguide.model.Account;
import pl.touristguide.model.UserDetail;
import pl.touristguide.springapp.user.dto.AccountDTO;
import pl.touristguide.springapp.user.dto.UserDetailDTO;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Service
public class UserFacade {
    private final UserService userService;
    private final AuthenticationService authenticationService;

    @Autowired
    public UserFacade(UserService userService, AuthenticationService authenticationService) {
        this.userService = userService;
        this.authenticationService = authenticationService;
    }

    public UserDetailDTO registration(UserDetailDTO userDetailDTO) throws Exception {
        return userService.registration(userDetailDTO);
    }

    public UserDetailDTO login(AccountDTO accountDTO, HttpServletRequest request) throws Exception {
        return authenticationService.login(accountDTO, request);
    }

    public UserDetailDTO updateUserDetail(UserDetailDTO userDetailDTO) throws Exception {
        UserDetailDTO currentUser = findAccountDetails();
        userService.updateUserDetail(currentUser, userDetailDTO);
        return findAccountDetails();
    }

    public void deactivateAccount(HttpServletRequest request, HttpServletResponse response) throws Exception {
        Account currentAccount = authenticationService.findCurrentAccount();
        userService.deactivateAccount(currentAccount);
        logoutUser(request, response);
    }

    public Account findAccountById(Long accountId) throws Exception {
        return userService.findAccountById(accountId);
    }

    public UserDetail insertUserDetail(UserDetail userDetail) {
        return userService.insertUserDetail(userDetail);
    }

    public Account insertAccount(Account account) {
        return userService.insertAccount(account);
    }

    public UserDetailDTO findAccountDetails() {
        return authenticationService.findAccountDetails();
    }

    public void logoutUser(HttpServletRequest request, HttpServletResponse response) throws Exception {
        authenticationService.logoutUser(request, response);
    }

    public void updateUserAccountPassword(AccountDTO accountDTO) throws Exception {
        Account currentAccount = authenticationService.findCurrentAccount();
        userService.updateUserAccountPassword(currentAccount, accountDTO);
    }
}
