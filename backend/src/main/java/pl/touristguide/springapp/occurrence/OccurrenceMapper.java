package pl.touristguide.springapp.occurrence;

import pl.touristguide.model.Account;
import pl.touristguide.model.Occurrence;
import pl.touristguide.model.Route;
import pl.touristguide.springapp.occurrence.dto.OccurrenceDTO;

public class OccurrenceMapper {

    public static Occurrence toOccurrence(OccurrenceDTO occurrenceDTO, Account account, Route route) {
        Occurrence occurrence = new Occurrence();
        occurrence.setOccurrenceId(occurrenceDTO.getOccurrenceId());
        occurrence.setTitle(occurrenceDTO.getTitle());
        occurrence.setStartDate(occurrenceDTO.getStartDate());
        occurrence.setEndDate(occurrenceDTO.getEndDate());
        occurrence.setAccount(account);
        occurrence.setRoute(route);
        return occurrence;
    }

    public static OccurrenceDTO toOccurrenceDTO(Occurrence occurrence) {
        OccurrenceDTO occurrenceDTO = new OccurrenceDTO();
        occurrenceDTO.setOccurrenceId(occurrence.getOccurrenceId());
        occurrenceDTO.setTitle(occurrence.getTitle());
        occurrenceDTO.setStartDate(occurrence.getStartDate());
        occurrenceDTO.setEndDate(occurrence.getEndDate());
        occurrenceDTO.setAccountId(occurrence.getAccount().getAccountId());
        occurrenceDTO.setRouteId(occurrence.getRoute().getRouteId());
        return occurrenceDTO;
    }
}
