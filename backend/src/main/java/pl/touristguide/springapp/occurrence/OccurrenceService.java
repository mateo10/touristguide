package pl.touristguide.springapp.occurrence;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.touristguide.model.Account;
import pl.touristguide.model.Occurrence;
import pl.touristguide.model.Route;
import pl.touristguide.springapp.occurrence.dto.OccurrenceDTO;
import pl.touristguide.springapp.route.RouteFacade;
import pl.touristguide.springapp.user.UserFacade;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
class OccurrenceService {
    private final OccurrenceDao occurrenceDao;
    private final UserFacade userFacade;
    private final RouteFacade routeFacade;

    @Autowired
    OccurrenceService(OccurrenceDao occurrenceDao, UserFacade userFacade, RouteFacade routeFacade) {
        this.occurrenceDao = occurrenceDao;
        this.userFacade = userFacade;
        this.routeFacade = routeFacade;
    }

    List<OccurrenceDTO> findUserOccurrences(Long accountId, LocalDate date) throws Exception {
        LocalDate firstDayOfMonth = date.withDayOfMonth(1);
        LocalDate lastDayOfMonth = date.withDayOfMonth(date.lengthOfMonth());
        Account account = userFacade.findAccountById(accountId);
        return account.getOccurrences().stream()
                .filter(occurrence -> !occurrence.getStartDate().isBefore(firstDayOfMonth))
                .filter(occurrence -> !occurrence.getEndDate().isAfter(lastDayOfMonth))
                .map(OccurrenceMapper::toOccurrenceDTO)
                .collect(Collectors.toList());
    }

    OccurrenceDTO insertOrUpdateOccurrence(OccurrenceDTO occurrenceDTO) throws Exception {
        Account account = userFacade.findAccountById(occurrenceDTO.getAccountId());
        Route route = routeFacade.getRouteModel(occurrenceDTO.getRouteId());

        if(occurrenceDTO.getEndDate() == null)
            occurrenceDTO.setEndDate(occurrenceDTO.getStartDate());

        Occurrence occurrence = occurrenceDao.save(OccurrenceMapper.toOccurrence(occurrenceDTO, account, route));
        return OccurrenceMapper.toOccurrenceDTO(occurrence);
    }

    void deleteOccurrence(Long occurrenceId) {
        occurrenceDao.deleteById(occurrenceId);
    }
}
