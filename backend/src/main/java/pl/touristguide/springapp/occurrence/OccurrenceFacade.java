package pl.touristguide.springapp.occurrence;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.touristguide.springapp.occurrence.dto.OccurrenceDTO;

import java.time.LocalDate;
import java.util.List;

@Service
public class OccurrenceFacade {
    private final OccurrenceService occurrenceService;

    @Autowired
    public OccurrenceFacade(OccurrenceService occurrenceService) {
        this.occurrenceService = occurrenceService;
    }

    public List<OccurrenceDTO> findUserOccurrences(Long accountId, LocalDate date) throws Exception {
       return occurrenceService.findUserOccurrences(accountId, date);
    }

    public OccurrenceDTO insertOrUpdateOccurrence(OccurrenceDTO occurrenceDTO) throws Exception {
        return occurrenceService.insertOrUpdateOccurrence(occurrenceDTO);
    }

    public void deleteOccurrence(Long occurrenceId) {
        occurrenceService.deleteOccurrence(occurrenceId);
    }
}
