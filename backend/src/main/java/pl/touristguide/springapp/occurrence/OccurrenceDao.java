package pl.touristguide.springapp.occurrence;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.touristguide.model.Occurrence;

@Repository
interface OccurrenceDao extends CrudRepository<Occurrence, Long> {
}
