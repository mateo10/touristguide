package pl.touristguide.model;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.time.LocalDateTime;

@Entity
@Table(name = "PLACE_EVALUATION")
public class PlaceEvaluation {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "PLACE_EVALUATION_ID")
    private Long placeEvaluationId;

    @Column(name = "COMMENTARY", length = 1024)
    private String commentary;

    @Min(0)
    @Max(5)
    @Column(name = "RATE")
    private Integer rate;

    @Column(name = "INSERTION_DATE")
    private LocalDateTime insertionDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PLACE_ID")
    private Place place;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ACCOUNT_ID")
    private Account account;

    public PlaceEvaluation() {
    }

    public Long getPlaceEvaluationId() {
        return placeEvaluationId;
    }

    public void setPlaceEvaluationId(Long placeEvaluationId) {
        this.placeEvaluationId = placeEvaluationId;
    }

    public String getCommentary() {
        return commentary;
    }

    public void setCommentary(String commentary) {
        this.commentary = commentary;
    }

    public Integer getRate() {
        return rate;
    }

    public void setRate(Integer rate) {
        this.rate = rate;
    }

    public LocalDateTime getInsertionDate() {
        return insertionDate;
    }

    public void setInsertionDate(LocalDateTime insertionDate) {
        this.insertionDate = insertionDate;
    }

    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }
}
