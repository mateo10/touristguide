package pl.touristguide.model;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.time.LocalDateTime;

@Entity
@Table(name = "ROUTE_EVALUATION")
public class RouteEvaluation {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ROUTE_EVALUATION_ID")
    private Long routeEvaluationId;

    @Column(name = "COMMENTARY", length = 1024)
    private String commentary;

    @Min(0)
    @Max(5)
    @Column(name = "RATE")
    private Integer rate;

    @Column(name = "INSERTION_DATE")
    private LocalDateTime insertionDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ROUTE_ID")
    private Route route;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ACCOUNT_ID")
    private Account account;

    public RouteEvaluation() {
    }

    public Long getRouteEvaluationId() {
        return routeEvaluationId;
    }

    public void setRouteEvaluationId(Long routeEvaluationId) {
        this.routeEvaluationId = routeEvaluationId;
    }

    public String getCommentary() {
        return commentary;
    }

    public void setCommentary(String commentary) {
        this.commentary = commentary;
    }

    public Integer getRate() {
        return rate;
    }

    public void setRate(Integer rate) {
        this.rate = rate;
    }

    public Route getRoute() {
        return route;
    }

    public LocalDateTime getInsertionDate() {
        return insertionDate;
    }

    public void setInsertionDate(LocalDateTime insertionDate) {
        this.insertionDate = insertionDate;
    }

    public void setRoute(Route route) {
        this.route = route;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }
}
