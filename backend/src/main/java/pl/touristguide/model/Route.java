package pl.touristguide.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "ROUTE")
public class Route {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ROUTE_ID")
    private Long routeId;

    @Column(name = "NAME", nullable = false)
    private String name;

    @Column(name = "DISTANCE", nullable = false)
    private Long distance;

    @Column(name = "DURATION", nullable = false)
    private Long duration;

    @Column(name = "ACTIVE")
    private boolean active;

    @Column(name = "PRIVATE")
    private boolean isPrivate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ACCOUNT_ID")
    private Account account;

    @ManyToMany
    @JoinTable(name = "ROUTE_PLACE", joinColumns = {@JoinColumn(name = "ROUTE_ID")}, inverseJoinColumns = {@JoinColumn(name = "PLACE_ID")})
    private List<Place> places;

    @OneToMany(mappedBy = "route", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private List<Occurrence> occurrences;

    @OneToMany(mappedBy = "route", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private List<RouteEvaluation> evaluations;

    public Route() {
    }

    public Long getRouteId() {
        return routeId;
    }

    public void setRouteId(Long routeId) {
        this.routeId = routeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getDistance() {
        return distance;
    }

    public void setDistance(Long distance) {
        this.distance = distance;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isPrivate() {
        return isPrivate;
    }

    public void setPrivate(boolean isPrivate) {
        this.isPrivate = isPrivate;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public List<Place> getPlaces() {
        return places;
    }

    public void setPlaces(List<Place> places) {
        this.places = places;
    }

    public List<Occurrence> getOccurrences() {
        return occurrences;
    }

    public void setOccurrences(List<Occurrence> occurrences) {
        this.occurrences = occurrences;
    }

    public List<RouteEvaluation> getEvaluations() {
        return evaluations;
    }

    public void setEvaluations(List<RouteEvaluation> evaluations) {
        this.evaluations = evaluations;
    }
}
