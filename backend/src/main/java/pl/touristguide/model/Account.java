package pl.touristguide.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "ACCOUNT")
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ACCOUNT_ID")
    private Long accountId;

    @Column(name = "USERNAME", nullable = false, unique = true)
    private String username;

    @Column(name = "PASSWORD", nullable = false)
    private String password;

    @Column(name = "ENABLED", nullable = false)
    private Boolean enabled;

    @Column(name = "ROLE", nullable = false)
    private String role;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USER_DETAIL_ID")
    private UserDetail userDetail;

    @OneToMany(mappedBy = "account", fetch = FetchType.LAZY)
    private List<Route> routes;

    @OneToMany(mappedBy = "account", fetch = FetchType.LAZY)
    private List<Place> places;

    @OneToMany(mappedBy = "account", fetch = FetchType.LAZY)
    private List<Occurrence> occurrences;

    @OneToMany(mappedBy = "account", fetch = FetchType.LAZY)
    private List<PlaceEvaluation> placeEvaluations;

    @OneToMany(mappedBy = "account", fetch = FetchType.LAZY)
    private List<RouteEvaluation> routeEvaluations;

    public Account() {
        this.routes = new ArrayList<>();
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public UserDetail getUserDetail() {
        return userDetail;
    }

    public void setUserDetail(UserDetail userDetail) {
        this.userDetail = userDetail;
    }

    public List<Route> getRoutes() {
        return routes;
    }

    public void setRoutes(List<Route> routes) {
        this.routes = routes;
    }

    public List<Place> getPlaces() {
        return places;
    }

    public void setPlaces(List<Place> places) {
        this.places = places;
    }

    public List<Occurrence> getOccurrences() {
        return occurrences;
    }

    public void setOccurrences(List<Occurrence> occurrences) {
        this.occurrences = occurrences;
    }

    public List<PlaceEvaluation> getPlaceEvaluations() {
        return placeEvaluations;
    }

    public void setPlaceEvaluations(List<PlaceEvaluation> placeEvaluations) {
        this.placeEvaluations = placeEvaluations;
    }

    public List<RouteEvaluation> getRouteEvaluations() {
        return routeEvaluations;
    }

    public void setRouteEvaluations(List<RouteEvaluation> routeEvaluations) {
        this.routeEvaluations = routeEvaluations;
    }
}
