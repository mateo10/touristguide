package groovy.pl.touristguide.evaluation


import pl.touristguide.springapp.evaluation.EvaluationSummaryService
import pl.touristguide.springapp.evaluation.dto.EvaluationDTO
import pl.touristguide.springapp.evaluation.dto.EvaluationsSummaryDTO
import spock.lang.Specification

import java.time.LocalDateTime

class EvaluationSummaryServiceTest extends Specification {
    private EvaluationSummaryService evaluationSummaryService = new EvaluationSummaryService()

    def "summarize null evaluations"() {
        given:
            List<EvaluationDTO> evaluations = null
        when:
            EvaluationsSummaryDTO summary = evaluationSummaryService.summarizeEvaluations(evaluations)
        then:
            summary.averageRating == 0
            summary.evaluationsQuantity == 0
            summary.fiveStars == 0
            summary.fourStars == 0
            summary.threeStars == 0
            summary.twoStars == 0
            summary.oneStar == 0
    }

    def "summarize empty evaluations"() {
        given:
            List<EvaluationDTO> evaluations = new ArrayList<>()
        when:
            EvaluationsSummaryDTO summary = evaluationSummaryService.summarizeEvaluations(evaluations)
        then:
            summary.averageRating == 0
            summary.evaluationsQuantity == 0
            summary.fiveStars == 0
            summary.fourStars == 0
            summary.threeStars == 0
            summary.twoStars == 0
            summary.oneStar == 0
    }

    def "summarize one evaluation"() {
        given:
            List<EvaluationDTO> evaluations = new ArrayList<>()
            evaluations.add(buildEvaluation(1, "test", 5))
        when:
            EvaluationsSummaryDTO summary = evaluationSummaryService.summarizeEvaluations(evaluations)
        then:
            summary.averageRating == 5
            summary.evaluationsQuantity == 1
            summary.fiveStars == 1
            summary.fourStars == 0
            summary.threeStars == 0
            summary.twoStars == 0
            summary.oneStar == 0
    }

    def "summarize many evaluations"() {
        given:
            List<EvaluationDTO> evaluations = new ArrayList<>()
            evaluations.add(buildEvaluation(1, "test1", 5))
            evaluations.add(buildEvaluation(2, "test2", 4))
            evaluations.add(buildEvaluation(3, "test3", 1))
            evaluations.add(buildEvaluation(3, "test3", 5))
        when:
            EvaluationsSummaryDTO summary = evaluationSummaryService.summarizeEvaluations(evaluations)
        then:
            summary.averageRating == 3.75
            summary.evaluationsQuantity == 4
            summary.fiveStars == 2
            summary.fourStars == 1
            summary.threeStars == 0
            summary.twoStars == 0
            summary.oneStar == 1
    }

    private static EvaluationDTO buildEvaluation(Long id, String comment, Integer rate) {
        EvaluationDTO evaluationDTO = new EvaluationDTO()
        evaluationDTO.setId(id)
        evaluationDTO.setComment(comment)
        evaluationDTO.setRate(rate)
        evaluationDTO.setInsertionDate(LocalDateTime.now())
        return evaluationDTO
    }
}
