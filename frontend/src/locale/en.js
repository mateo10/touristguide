export const en = {
  locale: {
    en: 'English',
    pl: 'Polish'
  },
  app_title: 'Tourist guide',
  add_place: 'Add place',
  add_route: 'Add route',
  routes: 'My routes',
  places: 'Places',
  categories: 'Categories',
  calendar: 'Schedule',
  my_account: 'My account',
  login: 'Log In',
  sign_up: 'Sign Up',
  logout: 'Logout',
  ownership: {
    MY: 'My',
    ALL: 'All'
  },
  order_field: {
    NAME_ASC: 'Name ascending',
    NAME_DESC: 'Name descending',
    RATE_ASC: 'Rate ascending',
    RATE_DESC: 'Rate descending'
  },
  place: {
    name: 'Name',
    description: 'Description',
    category: 'Category',
    website: 'Website address',
    location: 'Location',
    validation: {
      name_required: 'Name is required',
      description_required: 'Description is required',
      category_required: 'Category is required',
      location_required: 'Location is required'
    },
    add: 'Add place',
    edit: 'Edit place',
    added: 'Place added',
    updated: 'Place updated',
    deleted: 'Place removed',
    address: {
      title: 'Address',
      street: 'st'
    }
  },
  category: {
    name: 'Name',
    add: 'Add category',
    edit: 'Edit category',
    added: 'Category added',
    updated: 'Category updated',
    stateUpdate: 'Category state updated',
    exist_error: 'The category already exist',
    add_error: 'Add category error'
  },
  route: {
    name: 'Name',
    date: 'Date',
    distance: 'Distance',
    duration: 'Duration'
  },
  main: {
    add: 'Add',
    cancel: 'Cancel',
    edit: 'Edit',
    clear: 'Clear',
    close: 'Close',
    ok: 'OK',
    filter: 'Filter',
    return: 'Return',
    no_data: 'No data available',
    remove: 'Remove',
    minimum_number_characters: 'At least 8 characters'
  },
  map: {
    search: 'Search',
    map: 'Map',
    satellite: 'Satellite',
    terrain: 'Terrain',
    traffic: 'Traffic',
    public_transport: 'Public transport',
    cycle_paths: 'Cycle paths',
    route: 'Route'
  },
  user: {
    login: 'Login',
    old_password: 'Old password',
    password: 'Password',
    repeat_password: 'Repeat password',
    name: 'Name',
    email: 'Email',
    sign_in: 'Sign In',
    sign_up: 'Sign Up',
    remove: 'Delete account',
    edit: 'Edit account',
    change_password: 'Change password',
    detail: 'Account detail',
    account: 'Account',
    remove_desc: 'Are you sure you want to delete the account. The changes are irreversible'
  },
  evaluation: {
    title: 'Evaluations',
    evaluation: 'Evaluation',
    add_evaluation: 'Add evaluation'
  },
  schedule: {
    today: 'Today',
    calendar: {
      days: {
        0: 'Monday',
        1: 'Tuesday',
        2: 'Wednesday',
        3: 'Thursday',
        4: 'Friday',
        5: 'Saturday',
        6: 'Sunday'
      },
      months: {
        0: 'January',
        1: 'February',
        2: 'March',
        3: 'April',
        4: 'May',
        5: 'June',
        6: 'July',
        7: 'August',
        8: 'September',
        9: 'October',
        10: 'November',
        11: 'December'
      }
    },
    day_route: {
      title: 'Title',
      route: 'Route',
      date: 'Date'
    }
  },
  snackbar_message: {
    category: {
      insert_success: 'Category added',
      insert_error: 'Error adding category',
      update_success: 'The category has been edited',
      update_error: 'Error editing the category',
      state_update_success: 'Category status changed',
      state_update_error: 'Error during changed category status',
      exist_error: 'The category already exists',
      add_error: 'Error adding categories'
    },
    occurrence: {
      insert_success: 'An occurrence has been added',
      insert_error: 'Error adding occurrence',
      update_success: 'The occurrence was edited',
      update_error: 'Error editing the occurrence',
      remove_success: 'Occurrence removed',
      remove_error: 'Error while removing the occurrence'
    },
    place: {
      insert_success: 'An place has been added',
      insert_error: 'Error adding place',
      update_success: 'The place was edited',
      update_error: 'Error editing the place',
      remove_success: 'Place removed',
      remove_error: 'Error while removing the place'
    },
    route: {
      insert_success: 'Route has been added',
      insert_error: 'Error adding route',
      update_success: 'The route was edited',
      update_error: 'Error editing the route',
      remove_success: 'Route removed',
      remove_error: 'Error while removing the route',
      name_undefined: 'Route name is undefined'
    },
    evaluation: {
      insert_success: 'Evaluation has been added',
      insert_error: 'Error adding evaluation'
    },
    user: {
      different_passwords: 'Different passwords',
      login_error: 'Login error',
      logout_error: 'Logout error',
      registration_error: 'Registration error',
      update_error: 'Account update error',
      deactivate_error: 'Account delete error'
    }
  }
};
