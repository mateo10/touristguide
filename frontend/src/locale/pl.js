export const pl = {
  locale: {
    en: 'Angielski',
    pl: 'Polski'
  },
  app_title: 'Przewodnik turystyczny',
  add_place: 'Dodaj miejsce',
  add_route: 'Wyznacz trasę',
  routes: 'Moje trasy',
  places: 'Miejsca',
  categories: 'Kategorie',
  calendar: 'Terminarz',
  my_account: 'Moje konto',
  login: 'Zaloguj',
  sign_up: 'Utwórz konto',
  logout: 'Wyloguj',
  ownership: {
    MY: 'Moje',
    ALL: 'Wszystkie'
  },
  order_field: {
    NAME_ASC: 'Nazwa rosnąco',
    NAME_DESC: 'Nazwa malejąco',
    RATE_ASC: 'Ocena rosnąco',
    RATE_DESC: 'Ocena malejąco'
  },
  place: {
    name: 'Nazwa',
    description: 'Opis',
    category: 'Kategoria',
    website: 'Adres strony internetowej',
    location: 'Położenie',
    validation: {
      name_required: 'Nazwa jest wymagana',
      description_required: 'Opis jest wymagany',
      category_required: 'Kategoria jest wymagana',
      location_required: 'Położenie jest wymagane'
    },
    add: 'Dodaj miejsce',
    edit: 'Edytuj miejsce',
    added: 'Dodano miejsce',
    updated: 'Edytowano miejsce',
    deleted: 'Usunięto miejsce',
    address: {
      title: 'Adres',
      street: 'ul.'
    }
  },
  category: {
    name: 'Nazwa',
    add: 'Dodaj kategorię',
    edit: 'Edytuj kategorię'
  },
  route: {
    name: 'Nazwa',
    date: 'Data',
    distance: 'Dystans',
    duration: 'Czas'
  },
  main: {
    add: 'Dodaj',
    cancel: 'Anuluj',
    edit: 'Edytuj',
    clear: 'Wyczyść',
    close: 'Zamknij',
    ok: 'OK',
    filter: 'Filtruj',
    return: 'Wróć',
    no_data: 'Brak dostępnych danych',
    remove: 'Usuń',
    minimum_number_characters: 'Minimum 8 znaków'
  },
  map: {
    search: 'Wyszukaj',
    map: 'Mapa',
    satellite: 'Satelita',
    terrain: 'Teren',
    traffic: 'Natężenie ruchu',
    public_transport: 'Transport publiczny',
    cycle_paths: 'Trasy rowerowe',
    route: 'Trasa'
  },
  user: {
    login: 'Login',
    old_password: 'Stare hasło',
    password: 'Hasło',
    repeat_password: 'Powtórz hasło',
    name: 'Imię i nazwisko',
    email: 'Email',
    sign_in: 'Zaloguj',
    sign_up: 'Utwórz konto',
    remove: 'Usuń konto',
    edit: 'Edytuj konto',
    change_password: 'Zmień hasło',
    detail: 'Szczegóły',
    account: 'Konto',
    remove_desc: 'Czy na pewno chcesz usunąć konto. Zmiany są nieodwracalne'
  },
  evaluation: {
    title: 'Opinie',
    evaluation: 'Opinia',
    add_evaluation: 'Dodaj opinię'
  },
  schedule: {
    today: 'Dzisiaj',
    calendar: {
      days: {
        0: 'Poniedziałek',
        1: 'Wtorek',
        2: 'Środa',
        3: 'Czwartek',
        4: 'Piątek',
        5: 'Sobota',
        6: 'Niedziela'
      },
      months: {
        0: 'Styczeń',
        1: 'Luty',
        2: 'Marzec',
        3: 'Kwiecień',
        4: 'Maj',
        5: 'Czerwiec',
        6: 'Lipiec',
        7: 'Sierpień',
        8: 'Wrzesień',
        9: 'Październik',
        10: 'Listopad',
        11: 'Grudzień'
      }
    },
    day_route: {
      title: 'Tytuł',
      route: 'Trasa',
      date: 'Data'
    }
  },
  snackbar_message: {
    category: {
      insert_success: 'Dodano kategorię',
      insert_error: 'Błąd podczas dodawania kategorii',
      update_success: 'Edytowano kategorię',
      update_error: 'Błąd podczas edycji kategorii',
      state_update_success: 'Zmieniono stan kategorii',
      state_update_error: 'Błąd podczas zmieniony stan kategorii',
      exist_error: 'Istnieje już podana kategoria',
      add_error: 'Błąd przy dodawaniu kategorii'
    },
    occurrence: {
      insert_success: 'Dodano zdarzenie',
      insert_error: 'Błąd podczas dodawania zdarzenia',
      update_success: 'Edytowano zdarzenie',
      update_error: 'Błąd podczas edycji zdarzenia',
      remove_success: 'Usunięto zdarzenie',
      remove_error: 'Błąd podczas usuwania zdarzenia'
    },
    place: {
      insert_success: 'Dodano miejsce',
      insert_error: 'Błąd podczas dodawania miejsca',
      update_success: 'Edytowano miejsce',
      update_error: 'Błąd podczas edycji miejsca',
      remove_success: 'Usunięto miejsce',
      remove_error: 'Błąd podczas usuwania miejsca'
    },
    route: {
      insert_success: 'Dodano trasę',
      insert_error: 'Błąd podczas dodawania trasy',
      update_success: 'Edytowano trasę',
      update_error: 'Błąd podczas edycji trasy',
      remove_success: 'Usunięto trasę',
      remove_error: 'Błąd podczas usuwania trasy',
      name_undefined: 'Należy podać nazwę trasy'
    },
    evaluation: {
      insert_success: 'Dodano ocenę',
      insert_error: 'Błąd podczas dodawania oceny'
    },
    user: {
      different_passwords: 'Podane hasła różnią się',
      login_error: 'Wystąpił błąd przy próbie logowania',
      logout_error: 'Wystąpił błąd przy próbie wylogowania',
      registration_error: 'Wystąpił błąd przy rejestracji',
      update_error: 'Wystąpił błąd aktualizowaniu konta',
      deactivate_error: 'Wystąpił błąd przy usuwaniu konta'
    }
  }
};
