import '@fortawesome/fontawesome-free/css/all.css';
import 'vuetify/dist/vuetify.min.css';

import Vue from 'vue';
import VueResource from 'vue-resource';
import Vuetify from 'vuetify';
import VueI18n from 'vue-i18n';
import App from './App';
import router from './router';
import store from './store/store';
import { pl } from './locale/pl';
import { en } from './locale/en';

Vue.use(VueI18n);
const i18n = new VueI18n({
  locale: 'pl',
  messages: {
    pl,
    en
  }
});

Vue.use(Vuetify, {
  iconfont: 'fa'
});

Vue.use(VueResource);
Vue.http.options.root = 'http://localhost:9090/touristguide/rest/';
Vue.http.options.credentials = true;
Vue.config.productionTip = false;

Vue.filter('toKilometers', (value) => {
  if (!value) {
    return 0;
  }
  return Number.parseFloat(value / 1000).toFixed(2) + ' km';
});

Vue.filter('convertSeconds', (value) => {
  if (!value) {
    return 0;
  }
  const date = new Date(null);
  date.setSeconds(value);
  return date.toISOString().substr(11, 8);
});

new Vue({
  i18n,
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
});
