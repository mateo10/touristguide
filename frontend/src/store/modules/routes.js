import Vue from 'vue';

const state = {
  routes: []
};

const mutations = {
  setRoutes(state, routes) {
    state.routes = routes;
  },
  insertRoute(state, route) {
    state.routes.push(route);
  },
  updateRoute(state, route) {
    state.routes.forEach((value) => {
      if (value.routeId === route.routeId) {
        Object.assign(value, route);
      }
    });
  },
  removeRoute(state, routeId) {
    state.routes.splice(state.routes.findIndex(value =>
      value.routeId === routeId), 1);
  },
  insertRouteEvaluation(state, response) {
    state.routes.forEach((route) => {
      if (route.routeId === response.routeId) {
        route.evaluations.push(response.evaluation);
      }
    });
  }
};

const actions = {
  loadActiveRoutes: ({ commit }) => {
    Vue.http.get('./routes/active')
      .then(response => response.json())
      .then((result) => {
        if (result) {
          commit('setRoutes', result);
        }
      }, (error) => {
        console.log(error);
      });
  },
  loadUserRoutes: ({ commit }, accountId) => {
    Vue.http.get(`./routes/user/${accountId}`)
      .then(response => response.json())
      .then((result) => {
        if (result) {
          commit('setRoutes', result);
        }
      }, (error) => {
        console.log(error);
      });
  },
  insertRoute: ({ commit, dispatch }, route) => {
    Vue.http.post('./routes', route)
      .then(response => response.json())
      .then((response) => {
        commit('insertRoute', response);
        dispatch('addSuccessSnackbar', 'route.insert_success');
      }, (error) => {
        dispatch('addErrorSnackbar', 'route.insert_error');
      });
  },
  updateRoute: ({ commit, dispatch }, route) => {
    Vue.http.patch(`./routes/${route.routeId}`, route)
      .then(response => response.json())
      .then((response) => {
        commit('updateRoute', response);
        dispatch('addSuccessSnackbar', 'route.update_success');
      }, (error) => {
        dispatch('addErrorSnackbar', 'route.update_error');
      });
  },
  removeRoute: ({ commit, dispatch }, routeId) => {
    Vue.http.delete(`./routes/${routeId}`)
      .then((response) => {
        commit('removeRoute', routeId);
        dispatch('addSuccessSnackbar', 'route.remove_success');
      }, (error) => {
        dispatch('addErrorSnackbar', 'route.remove_error');
      });
  },
  insertRouteEvaluation: ({ commit, dispatch }, evaluation) => {
    Vue.http.post('./routes/evaluations', evaluation)
      .then(response => response.json())
      .then((response) => {
        commit('insertRouteEvaluation', {evaluation: response, routeId: evaluation.routeId});
        dispatch('addSuccessSnackbar', 'evaluation.insert_success');
      }, (error) => {
        dispatch('addErrorSnackbar', 'evaluation.insert_error');
      });
  },
  searchRoutes: ({ commit }, routeFilter) => {
    Vue.http.post('./routes/search', routeFilter)
      .then(response => response.json())
      .then((result) => {
        if (result) {
          commit('setRoutes', result);
        }
      }, (error) => {
        console.log(error);
      });
  }
};

const getters = {
  routes: state => state.routes,
  getRouteById: state => routeId => state.routes.find(route => route.routeId === routeId)
};

export default {
  state,
  mutations,
  actions,
  getters
};
