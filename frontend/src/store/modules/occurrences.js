import Vue from 'vue';

const state = {
  occurrences: []
};

const mutations = {
  setOccurrences(state, occurrences) {
    state.occurrences = occurrences;
  },
  insertOccurrence(state, occurrence) {
    state.occurrences.push(occurrence);
  },
  updateOccurrence(state, occurrence) {
    state.occurrences.forEach((value) => {
      if (value.occurrenceId === occurrence.occurrenceId) {
        Object.assign(value, occurrence);
      }
    });
  },
  removeOccurrence(state, occurrenceId) {
    state.occurrences.splice(state.occurrences.findIndex(value =>
      value.occurrenceId === occurrenceId), 1);
  }
};

const actions = {
  loadOccurrences: ({ commit }, params) => {
    Vue.http.post(`./occurrences/user/${params.accountId}`, params.date)
      .then(response => response.json())
      .then((result) => {
        if (result) {
          commit('setOccurrences', result);
        }
        params.resolve();
      }, (error) => {
        params.reject();
      });
  },
  insertOccurrence: ({ commit, dispatch }, occurrence) => {
    Vue.http.patch('./occurrences', occurrence)
      .then(response => response.json())
      .then((response) => {
        commit('insertOccurrence', response);
        dispatch('addSuccessSnackbar', 'occurrence.insert_success');
      }, (error) => {
        dispatch('addErrorSnackbar', 'occurrence.insert_error');
      });
  },
  updateOccurrence: ({ commit, dispatch }, occurrence) => {
    Vue.http.patch('./occurrences', occurrence)
      .then(response => response.json())
      .then((response) => {
        commit('updateOccurrence', response);
        dispatch('addSuccessSnackbar', 'occurrence.update_success');
      }, (error) => {
        dispatch('addErrorSnackbar', 'occurrence.update_error');
      });
  },
  removeOccurrence: ({ commit, dispatch }, occurrenceId) => {
    Vue.http.delete(`./occurrences/${occurrenceId}`)
      .then((response) => {
        commit('removeCategory', occurrenceId);
        dispatch('addSuccessSnackbar', 'occurrence.remove_success');
      }, (error) => {
        dispatch('addErrorSnackbar', 'occurrence.remove_error');
      });
  }
};

const getters = {
  occurrences(state) {
    return state.occurrences;
  }
};

export default {
  state,
  mutations,
  actions,
  getters
};
