import Vue from 'vue';

const state = {
  categories: []
};

const mutations = {
  setCategories(state, categories) {
    state.categories = categories;
  },
  insertCategory(state, category) {
    state.categories.push(category);
  },
  updateCategory(state, category) {
    state.categories.forEach((value) => {
      if (value.categoryId === category.categoryId) {
        Object.assign(value, category);
      }
    });
  },
  removeCategory(state, categoryId) {
    state.categories.splice(state.categories.findIndex(value =>
      value.categoryId === categoryId), 1);
  }
};

const actions = {
  loadCategories: ({ commit }) => {
    Vue.http.get('./categories')
      .then(response => response.json())
      .then((result) => {
        if (result) {
          commit('setCategories', result);
        }
      });
  },
  loadActiveCategories: ({ commit }) => {
    Vue.http.get('./categories/active')
      .then(response => response.json())
      .then((result) => {
        if (result) {
          commit('setCategories', result);
        }
      });
  },
  insertCategory: ({ commit, dispatch }, category) => {
    Vue.http.post('./categories', category)
      .then(response => response.json())
      .then((response) => {
        commit('insertCategory', response);
        dispatch('addSuccessSnackbar', 'category.insert_success');
      }, (error) => {
        dispatch('addErrorSnackbar', 'category.insert_error');
      });
  },
  updateCategory: ({ commit, dispatch }, category) => {
    Vue.http.patch(`./categories/${category.categoryId}`, category)
      .then(response => response.json())
      .then((response) => {
        commit('updateCategory', response);
        dispatch('addSuccessSnackbar', 'category.update_success');
      }, (error) => {
        dispatch('addErrorSnackbar', 'category.update_error');
      });
  },
  updateCategoryState: ({ commit, dispatch }, category) => {
    Vue.http.patch(`./categories/${category.categoryId}/state`, category.state)
      .then((response) => {
        commit('updateCategory', response);
        dispatch('addSuccessSnackbar', 'category.state_update_success');
      }, (error) => {
        dispatch('addErrorSnackbar', 'category.state_update_error');
      });
  }
};

const getters = {
  categories(state) {
    return state.categories;
  }
};

export default {
  state,
  mutations,
  actions,
  getters
};
