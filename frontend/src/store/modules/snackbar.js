const state = {
  statement: {
    snackbar: false,
    color: '',
    message: ''
  }
};

const mutations = {
  setStatement(state, statement) {
    state.statement = statement;
  },
  disableSnackbar(state) {
    state.statement.snackbar = false;
  }
};

const actions = {
  setStatement: ({ commit }, statement) => {
    commit('setStatement', statement);
  },
  addSuccessSnackbar: ({ commit }, message) => {
    commit('setStatement', {
      snackbar: true,
      message,
      color: 'success'
    });
  },
  addWarningSnackbar: ({ commit }, message) => {
    commit('setStatement', {
      snackbar: true,
      message,
      color: 'warning'
    });
  },
  addErrorSnackbar: ({ commit }, message) => {
    commit('setStatement', {
      snackbar: true,
      message,
      color: 'error'
    });
  },
  disableSnackbar: ({ commit }) => {
    commit('disableSnackbar');
  }
};

const getters = {
  getStatement(state) {
    return state.statement;
  }
};

export default {
  state,
  mutations,
  actions,
  getters
};
