import Vue from 'vue';
import router from '../../router';

const state = {
  currentUser: null,
  isLogged: false
};

const mutations = {
  setCurrentUser(state, user) {
    state.currentUser = user;
    state.isLogged = true;
  },
  removeCurrentUser(state) {
    state.currentUser = null;
    state.isLogged = false;
  }
};

const actions = {
  signIn: ({ commit, dispatch }, user) => {
    Vue.http.post('./user/login', user)
      .then(result => result.json())
      .then((result) => {
        commit('setCurrentUser', result);
        router.push({ name: 'Places' });
      }, (error) => {
        dispatch('addErrorSnackbar', 'user.login_error');
      });
  },
  signOut: ({ commit, dispatch }) => {
    Vue.http.post('./user/logout')
      .then((result) => {
        commit('removeCurrentUser');
        router.push({ name: 'Places' });
      }, (error) => {
        dispatch('addErrorSnackbar', 'user.logout_error');
      });
  },
  signUp: ({ commit, dispatch }, user) => {
    Vue.http.post('./user/signup', user)
      .then(result => result.json())
      .then((result) => {
        commit('setCurrentUser', result);
        router.push({ name: 'Places' });
      }, (error) => {
        dispatch('addErrorSnackbar', 'user.registration_error');
      });
  },
  getCurrentUserDetail: ({ commit }) => {
    Vue.http.get('./user/detail')
      .then(result => result.json())
      .then((result) => {
        if (result) {
          commit('setCurrentUser', result);
        }
      }, (error) => {
        console.error('not logged user');
      });
  },
  updateUserDetail: ({ commit, dispatch }, user) => {
    Vue.http.patch('./user', user)
      .then(result => result.json())
      .then((result) => {
        if (result) {
          commit('setCurrentUser', result);
        }
      }, (error) => {
        dispatch('addErrorSnackbar', 'user.update_error');
      });
  },
  updatePassword: ({ commit, dispatch }, account) => {
    Vue.http.patch('./user/password', account)
      .then(result => result.json())
      .then((result) => {
        if (result) {
          commit('setCurrentUser', result);
        }
      }, (error) => {
        dispatch('addErrorSnackbar', 'user.update_error');
      });
  },
  deactivateAccount: ({ commit, dispatch }) => {
    Vue.http.delete('./user')
      .then((result) => {
        commit('removeCurrentUser');
        router.push({ name: 'Places' });
      }, (error) => {
        dispatch('addErrorSnackbar', 'user.deactivate_error');
      });
  }
};

const getters = {
  currentUser(state) {
    return state.currentUser;
  },
  isLogged(state) {
    return state.isLogged;
  }
};

export default {
  state,
  mutations,
  actions,
  getters
};
