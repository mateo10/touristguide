import Vue from 'vue';

const state = {
  places: []
};

const mutations = {
  setPlaces(state, places) {
    state.places = places;
  },
  insertPlace(state, place) {
    state.places.push(place);
  },
  updatePlace(state, place) {
    state.places.forEach((value) => {
      if (value.placeId === place.placeId) {
        Object.assign(value, place);
      }
    });
  },
  removePlace(state, placeId) {
    state.places.splice(state.places.findIndex(value =>
      value.placeId === placeId), 1);
  },
  insertPlaceEvaluation(state, response) {
    state.places.forEach((place) => {
      if (place.placeId === response.placeId) {
        place.evaluations.push(response.evaluation);
      }
    });
  }
};

const actions = {
  loadPlaces: ({ commit }) => {
    Vue.http.get('./places')
      .then(response => response.json())
      .then((result) => {
        if (result) {
          commit('setPlaces', result);
        }
      }, (error) => {
        console.log(error);
      });
  },
  insertPlace: ({ commit, dispatch }, place) => {
    Vue.http.post('./places', place)
      .then(response => response.json())
      .then((response) => {
        commit('insertPlace', response);
        dispatch('addSuccessSnackbar', 'place.insert_success');
      }, (error) => {
        dispatch('addErrorSnackbar', 'place.insert_error');
      });
  },
  updatePlace: ({ commit, dispatch }, place) => {
    Vue.http.patch(`./places/${place.placeId}`, place)
      .then(response => response.json())
      .then((response) => {
        commit('updatePlace', response);
        dispatch('addSuccessSnackbar', 'place.update_success');
      }, (error) => {
        dispatch('addErrorSnackbar', 'place.update_error');
      });
  },
  removePlace: ({ commit, dispatch }, placeId) => {
    Vue.http.delete(`./places/${placeId}`)
      .then((response) => {
        commit('removePlace', placeId);
        dispatch('addSuccessSnackbar', 'place.remove_success');
      }, (error) => {
        dispatch('addErrorSnackbar', 'place.remove_error');
      });
  },
  insertPlaceEvaluation: ({ commit, dispatch }, evaluation) => {
    Vue.http.post('./places/evaluations', evaluation)
      .then(response => response.json())
      .then((response) => {
        commit('insertPlaceEvaluation', {evaluation: response, placeId: evaluation.placeId});
        dispatch('addSuccessSnackbar', 'evaluation.insert_success');
      }, (error) => {
        dispatch('addErrorSnackbar', 'evaluation.insert_error');
      });
  },
  searchPlaces: ({ commit }, placeFilter) => {
    Vue.http.post('./places/search', placeFilter)
      .then(response => response.json())
      .then((result) => {
        if (result) {
          commit('setPlaces', result);
        }
      }, (error) => {
        console.log(error);
      });
  }
};

const getters = {
  places: state => state.places,
  getPlaceById: state => placeId => state.places.find(place => place.placeId === placeId)
};

export default {
  state,
  mutations,
  actions,
  getters
};
