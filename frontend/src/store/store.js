import Vuex from 'vuex';
import Vue from 'vue';

import places from './modules/places';
import categories from './modules/categories';
import routes from './modules/routes';
import authorization from './modules/authorization';
import occurrences from './modules/occurrences';
import snackbar from './modules/snackbar';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    places,
    categories,
    routes,
    authorization,
    occurrences,
    snackbar
  }
});
