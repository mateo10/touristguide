import Vue from 'vue';
import Router from 'vue-router';
import HelloWorld from '@/components/HelloWorld';
import Places from '@/components/places/Places';
import Place from '@/components/places/Place';
import AddRoute from '@/components/routes/AddRoute';
import Routes from '@/components/routes/Routes';
import Categories from '@/components/categories/Categories';
import UserLogin from '@/components/account/UserLogin';
import UserSignUp from '@/components/account/UserSignUp';
import Account from '@/components/account/Account';
import Schedule from '@/components/schedule/Schedule';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/places',
      name: 'Places',
      component: Places
    },
    {
      path: '/places/:placeId',
      name: 'Place',
      component: Place
    },
    {
      path: '/places/add',
      name: 'AddPlace',
      component: Place
    },
    {
      path: '/places/:placeId/edit',
      name: 'EditPlace',
      component: Place
    },
    {
      path: '/routes/add',
      name: 'AddRoute',
      component: AddRoute
    },
    {
      path: '/routes/:routeId',
      name: 'Route',
      component: AddRoute
    },
    {
      path: '/routes/:routeId/edit',
      name: 'EditRoute',
      component: AddRoute
    },
    {
      path: '/routes',
      name: 'Routes',
      component: Routes
    },
    {
      path: '/categories',
      name: 'Categories',
      component: Categories
    },
    {
      path: '/login',
      name: 'UserLogin',
      component: UserLogin
    },
    {
      path: '/signup',
      name: 'UserSignUp',
      component: UserSignUp
    },
    {
      path: '/account',
      name: 'Account',
      component: Account
    },
    {
      path: '/schedule',
      name: 'Schedule',
      component: Schedule
    }
  ]
});
